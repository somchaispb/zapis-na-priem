<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTicketbookedbyToAppointmentticket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appointmentticket', function($table) {
            $table->integer('ticketbookedby')->default('1');
            $table->foreign('ticketbookedby')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointmentticket', function($table) {
            $table->dropColumn('ticketbookedby');
        });
    }
}
