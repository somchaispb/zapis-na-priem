<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateVEmpltabView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('v_empltab', function (Blueprint $table) {
            DB::statement('
                create or replace view v_empltab as
                SELECT r.ticketorganization,
                         orgn.name           AS orgname,
                         r.specspeciality,
                         spec.name,
                         r.specfullname,
                         sum(r.freeticket)   AS freeticket,
                         sum(r.bookedticket) AS bookedticket
                  FROM (((SELECT r_1.ticketorganization,
                                 r_1.specspeciality,
                                 r_1.specfullname,
                                 CASE
                                   WHEN (r_1.ticketstatus = 1) THEN r_1.cnt
                                   ELSE (0) :: bigint
                                     END AS freeticket,
                                 CASE
                                   WHEN (r_1.ticketstatus = 2) THEN r_1.cnt
                                   ELSE (0) :: bigint
                                     END AS bookedticket
                          FROM (SELECT aptc.ticketorganization,
                                       aptc.specspeciality,
                                       aptc.ticketstatus,
                                       aptc.specfullname,
                                       count(*) AS cnt
                                FROM (appointmentticket aptc
                                    JOIN dates dats ON (((dats.id = 1) AND ((aptc.ticketdate >= dats.datefr) AND
                                                                                        (aptc.ticketdate <= dats.dateto)))))
                                GROUP BY aptc.ticketorganization, aptc.specspeciality, aptc.specfullname, aptc.ticketstatus) r_1) r
                      LEFT JOIN d_organization orgn ON ((orgn.organizationid = r.ticketorganization)))
                      LEFT JOIN d_speciality spec ON ((spec.specialityid = r.specspeciality)))
                  GROUP BY r.ticketorganization, orgn.name, r.specspeciality, spec.name, r.specfullname;
            ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_empltab');
    }
}
