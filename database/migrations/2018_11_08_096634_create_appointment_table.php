<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAppointmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointmentticket', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'ru_RU.UTF-8';
            $table->unsignedBigInteger('appointmentticketid');
            $table->bigInteger('ticketstatus');
            $table->foreign('ticketstatus')->references('ticketstatusid')->on('d_ticketstatus');
            $table->string('ticketnumber', 100)->nullable();
            $table->string('ticketnumberexternal', 100)->nullable();
            $table->timestampTz('ticketpublicationdate')->nullable()->default('now()');
            $table->bigInteger('ticketorganization')->nullable();
            $table->foreign('ticketorganization')->references('organizationid')->on('d_organization');
            $table->bigInteger('ticketcreatedby')->default(1);
            $table->foreign('ticketcreatedby')->references('id')->on('users');
            $table->timestampTz('ticketcreateddate')->nullable()->default('now()');
            $table->bigInteger('ticketmodifiedby')->nullable()->default(1);
            $table->timestampTz('ticketmodifieddate')->nullable()->default('now()');
            $table->string('specfullname', 300)->nullable();
            $table->string('speclastname', 300)->nullable();
            $table->string('specfirstname', 100)->nullable();
            $table->string('specpatronymicname', 100)->nullable();
            $table->bigInteger('specspeciality')->nullable();
            $table->foreign('specspeciality')->references('specialityid')->on('d_speciality');
            $table->string('ticketservice', 100)->nullable();
            $table->date('ticketdate')->nullable();
            $table->time('tickettime')->nullable();
            $table->string('paymtype', 100)->nullable();
            $table->string('price', 100)->nullable();
            $table->string('address', 100)->nullable();
            $table->string('addressnear', 100)->nullable();
            $table->string('timeformoving', 100)->nullable();
            $table->text('recommendations')->nullable();
            $table->string('patfullname', 300)->nullable();
            $table->string('patlastname', 100)->nullable();
            $table->string('patfirstname', 100)->nullable();
            $table->string('patpatronymicname', 100)->nullable();
            $table->date('patbirthdate')->nullable();
            $table->string('patpolicynum', 100)->nullable();
            $table->date('patpolicydate')->nullable();
            $table->string('patphone', 100)->nullable();
            $table->string('patemail', 100)->nullable();
            $table->string('patvisitpurpose', 100)->nullable();
            $table->text('patcomment')->nullable();

            $table->unique('appointmentticketid', '');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointmentticket');
    }
}
