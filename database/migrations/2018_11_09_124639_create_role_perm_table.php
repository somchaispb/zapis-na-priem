<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolePermTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_perm', function (Blueprint $table) {
            $table->integer('role_id');
            $table->foreign('role_id')->references('role_id')->on('roles');
            $table->integer('perm_id');
            $table->foreign('perm_id')->references('perm_id')->on('permissions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_perm');
    }
}
