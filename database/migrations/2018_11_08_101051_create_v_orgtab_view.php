<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateVOrgtabView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('v_orgtab', function (Blueprint $table) {
            DB::statement('
                create or replace view v_orgtab as
                SELECT r.ticketorganization,
                         orgn.name                                                 AS orgname,
                         sum(r.freeticket)                                         AS freeticket,
                         sum(r.bookedticket)                                       AS bookedticket,
                         (SELECT count(DISTINCT aptc1.specfullname) AS count
                          FROM (appointmentticket aptc1
                              JOIN dates dats ON (((dats.id = 1) AND ((aptc1.ticketdate >= dats.datefr) AND
                                                                                  (aptc1.ticketdate <= dats.dateto)))))
                          WHERE (aptc1.ticketorganization = r.ticketorganization)) AS empl_qty,
                         (SELECT count(DISTINCT aptc2.specspeciality) AS count
                          FROM (appointmentticket aptc2
                              JOIN dates dats ON (((dats.id = 1) AND ((aptc2.ticketdate >= dats.datefr) AND
                                                                                  (aptc2.ticketdate <= dats.dateto)))))
                          WHERE (aptc2.ticketorganization = r.ticketorganization)) AS spec_qty
                  FROM ((SELECT r_1.ticketorganization,
                                CASE
                                  WHEN (r_1.ticketstatus = 1) THEN r_1.cnt
                                  ELSE (0) :: bigint
                                    END AS freeticket,
                                CASE
                                  WHEN (r_1.ticketstatus = 2) THEN r_1.cnt
                                  ELSE (0) :: bigint
                                    END AS bookedticket
                         FROM (SELECT aptc.ticketorganization, aptc.ticketstatus, count(*) AS cnt
                               FROM (appointmentticket aptc
                                   JOIN dates dats ON (((dats.id = 1) AND ((aptc.ticketdate >= dats.datefr) AND
                                                                                       (aptc.ticketdate <= dats.dateto)))))
                               GROUP BY aptc.ticketorganization, aptc.ticketstatus) r_1) r
                      LEFT JOIN d_organization orgn ON ((orgn.organizationid = r.ticketorganization)))
                  GROUP BY r.ticketorganization, orgn.name;
            ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_orgtab');
    }
}
