<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpecspeciality123ToAppointmentticket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appointmentticket', function($table) {
            $table->bigInteger('specspeciality1')->nullable();
            $table->foreign('specspeciality1')->references('specialityid')->on('d_speciality');
            $table->bigInteger('specspeciality2')->nullable();
            $table->foreign('specspeciality2')->references('specialityid')->on('d_speciality');
            $table->bigInteger('specspeciality3')->nullable();
            $table->foreign('specspeciality3')->references('specialityid')->on('d_speciality');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointmentticket', function($table) {
            $table->dropcolumn('specspeciality1');
            $table->dropcolumn('specspeciality2');
            $table->dropcolumn('specspeciality3');
        });
    }
}
