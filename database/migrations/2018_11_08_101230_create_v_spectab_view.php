<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateVSpectabView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('v_spectab', function (Blueprint $table) {
            DB::statement('
                create or replace view v_spectab as
                SELECT r.ticketorganization,
                         orgn.name                                                                                                 AS orgname,
                         r.specspeciality,
                         spec.name,
                         sum(r.freeticket)                                                                                         AS freeticket,
                         sum(r.bookedticket)                                                                                       AS bookedticket,
                         (SELECT count(DISTINCT aptc1.specfullname) AS count
                          FROM (appointmentticket aptc1
                              JOIN dates dats ON (((dats.id = 1) AND ((aptc1.ticketdate >= dats.datefr) AND
                                                                                  (aptc1.ticketdate <= dats.dateto)))))
                          WHERE ((aptc1.ticketorganization = r.ticketorganization) AND
                                 (aptc1.specspeciality = r.specspeciality)))                                                       AS empl_qty
                  FROM (((SELECT r_1.ticketorganization,
                                 r_1.specspeciality,
                                 CASE
                                   WHEN (r_1.ticketstatus = 1) THEN r_1.cnt
                                   ELSE (0) :: bigint
                                     END AS freeticket,
                                 CASE
                                   WHEN (r_1.ticketstatus = 2) THEN r_1.cnt
                                   ELSE (0) :: bigint
                                     END AS bookedticket
                          FROM (SELECT aptc.ticketorganization, aptc.specspeciality, aptc.ticketstatus, count(*) AS cnt
                                FROM (appointmentticket aptc
                                    JOIN dates dats ON (((dats.id = 1) AND ((aptc.ticketdate >= dats.datefr) AND
                                                                                        (aptc.ticketdate <= dats.dateto)))))
                                GROUP BY aptc.ticketorganization, aptc.specspeciality, aptc.ticketstatus) r_1) r
                      LEFT JOIN d_organization orgn ON ((orgn.organizationid = r.ticketorganization)))
                      LEFT JOIN d_speciality spec ON ((spec.specialityid = r.specspeciality)))
                  GROUP BY r.ticketorganization, orgn.name, r.specspeciality, spec.name;
            ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_spectab');
    }
}
