<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateVDatetabView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('v_datetab', function (Blueprint $table) {
            DB::statement('
            create or replace view v_datetab as
            SELECT r.ticketorganization,
                     orgn.name                                                                                                 AS orgname,
                     r.specspeciality,
                     spec.name,
                     r.specfullname,
                     r.ticketdate,
                     sum(r.freeticket)                                                                                         AS freeticket,
                     sum(r.bookedticket)                                                                                       AS bookedticket,
                     (SELECT min(aptc1.tickettime) AS min
                      FROM (appointmentticket aptc1
                          JOIN dates dats ON (((dats.id = 1) AND ((aptc1.ticketdate >= dats.datefr) AND
                                                                              (aptc1.ticketdate <= dats.dateto)))))
                      WHERE ((aptc1.ticketorganization = r.ticketorganization) AND (aptc1.specspeciality = r.specspeciality) AND
                             ((aptc1.specfullname) :: text = (r.specfullname) :: text) AND
                             (aptc1.ticketdate = r.ticketdate)))                                                               AS min_time,
                     (SELECT max(aptc1.tickettime) AS max
                      FROM (appointmentticket aptc1
                          JOIN dates dats ON (((dats.id = 1) AND ((aptc1.ticketdate >= dats.datefr) AND
                                                                              (aptc1.ticketdate <= dats.dateto)))))
                      WHERE ((aptc1.ticketorganization = r.ticketorganization) AND (aptc1.specspeciality = r.specspeciality) AND
                             ((aptc1.specfullname) :: text = (r.specfullname) :: text) AND
                             (aptc1.ticketdate = r.ticketdate)))                                                               AS max_time
              FROM (((SELECT r_1.ticketorganization,
                             r_1.specspeciality,
                             r_1.specfullname,
                             r_1.ticketdate,
                             CASE
                               WHEN (r_1.ticketstatus = 1) THEN r_1.cnt
                               ELSE (0) :: bigint
                                 END AS freeticket,
                             CASE
                               WHEN (r_1.ticketstatus = 2) THEN r_1.cnt
                               ELSE (0) :: bigint
                                 END AS bookedticket
                      FROM (SELECT aptc.ticketorganization,
                                   aptc.specspeciality,
                                   aptc.specfullname,
                                   aptc.ticketdate,
                                   aptc.ticketstatus,
                                   count(*) AS cnt
                            FROM (appointmentticket aptc
                                JOIN dates dats ON (((dats.id = 1) AND ((aptc.ticketdate >= dats.datefr) AND
                                                                                    (aptc.ticketdate <= dats.dateto)))))
                            GROUP BY aptc.ticketorganization, aptc.specspeciality, aptc.specfullname, aptc.ticketdate,
                                     aptc.ticketstatus) r_1) r
                  LEFT JOIN d_organization orgn ON ((orgn.organizationid = r.ticketorganization)))
                  LEFT JOIN d_speciality spec ON ((spec.specialityid = r.specspeciality)))
              GROUP BY r.ticketorganization, orgn.name, r.specspeciality, spec.name, r.specfullname, r.ticketdate;
            ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('v_datetab');
    }
}
