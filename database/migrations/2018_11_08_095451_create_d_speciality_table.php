<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDSpecialityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('d_speciality', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'ru_RU.UTF-8';
            $table->unsignedBigInteger('specialityid');
            $table->string('name', 100);
            $table->unique('specialityid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_speciality');
    }
}
