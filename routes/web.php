<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', 'PagesController@index');
Route::get('/find', 'PagesController@find');
Route::get('/add', 'PagesController@add');
Route::get('/show/{id}', 'PagesController@show');

Route::post('/find', 'AjaxController@saveForm');
Route::post('/ajaxform', 'AjaxController@ajaxForm');
Route::post('/add', 'PagesController@addForm');
Route::put('/updateticket', 'PagesController@updateticket');
Route::post('/savedate', 'AjaxController@saveDate');
Route::get('/servertime', 'PagesController@serverTime');
// Ticket operations
Route::delete('/ticket/delete/{id}', 'TicketController@delete');
Route::get('/ticket/update/{id}', 'TicketController@update');
Route::post('/ticket/cancel', 'TicketController@cancel');
Route::get('/ticket/all', 'TicketController@index');
Route::post('ticket/delete/bulk', 'TicketController@destroy');
Route::get('/ticket/edit/{id}', 'TicketController@edit');
Route::post('/ticket/edit/{id}', 'TicketController@editForm');
//if (file_exists(__DIR__.'app/Http/Controllers/ServerController.php')) {
//    Route::get('/deploy', 'ServerController@deploy');
//}
// REST
Route::post('/api/register', 'UserController@register');
Route::post('/api/login', 'UserController@authenticate');
Route::group([
    'prefix' => '/api',
    'middleware' => 'throttle:10',
                    ['jwt.verify']
    ], function() {
    Route::get('/user', 'UserController@getAuthenticatedUser');
    Route::get('/tickets', 'RestController@index');
    Route::get('/tickets/{id}', 'RestController@show');
    Route::post('/tickets', 'RestController@create');
    Route::delete('/tickets', 'RestController@destroy');
    Route::put('/tickets/{id}', 'RestController@edit');
    Route::get('/api/logout', 'UserController@logout');
});



