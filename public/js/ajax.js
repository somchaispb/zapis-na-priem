let items = [];
let out = false;
let token = $('meta[name=csrf-token]').attr("content");
let url = '/ajaxform';
let timeUrl = '/servertime';
let datesUrl = '/savedate';
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': token
    },
});
function closeWindow() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=UTF-8",
        url: timeUrl,
        data: {status: out}
    });
    return "Вы действительно хотите покинуть страницу?";
}
function countDown(response) {
    // Set the date we're counting down to
    let countDownDate = new Date(response).getTime();

// Update the count down every 1 second
    let x = setInterval(function() {

        // Get todays date and time
        let now = new Date().getTime();

        // Find the distance between now and the count down date
        let distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        let days = Math.floor(distance / (1000 * 60 * 60 * 24));
        let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);

        document.querySelector('.minutes').innerHTML = minutes;
        document.querySelector('.seconds').innerHTML = seconds;

        // If the count down is finished, write some text
        if (distance <= 0) {
            clearInterval(x);
            document.getElementById("clockdiv").innerHTML = "Период вашей сессии истек";
            out=true;
            closeWindow();
        }
    }, 1000);
}
// window.history.pushState({"page": "Clinic"},"", "find");
$(function () {
    $('input[name=tel]').mask('8(999)999-99-99');
    $('input[name=dob]').mask('99.99.9999', {
        // placeholder: "дд/мм.гггг",
    });
    $('input[name=policeexpires]').mask('99.99.9999');
    $('input[name=StartDate]').mask('99-99-9999');
    $('input[name=EndDate]').mask('99-99-9999');
    // Переключение по вкладкам на кнопке вперед
    // $('.tab-content').on('click', '.forward', function (e) {
    //     e.preventDefault();
    //     // window.history.forward();
    //     let activeTab = $('.tab-pane.active');
    //     let parentTabPane = $('.tab-pane.active').attr('id');
    //     let currentLink = $('.nav-link.active');
    //     let checked = [];
    //     $('input[name="multi"]:checked').each(function () {
    //         checked.push(this.value);
    //     });
    //     let data = {
    //         id: checked,
    //         ParentTab: parentTabPane
    //     };
    //     $.ajax({
    //         type: "POST",
    //         contentType: "application/json; charset=UTF-8",
    //         url: url,
    //         data: JSON.stringify(data),
    //         success: function(data) {
    //             $('#specspeciality table tbody').html(data.html);
    //         }
    //     });
    //     // Переключаем вкладки
    //     $(currentLink).attr("aria-selected","false").removeClass('active').parent().next('li').children().addClass('active').attr("aria-selected","true");
    //     $(activeTab).removeClass('active').next().addClass('active');
    // });
    // // Переключение по вкладкам на кнопке назад
    // $('.tab-content').on('click', '.back', function (e) {
    //     e.preventDefault();
    //     // window.history.back();
    //     let activeTab = $('.tab-pane.active');
    //     let currentLink = $('.nav-link.active');
    //     // Переключаем вкладки
    //     $(currentLink).attr("aria-selected","false").removeClass('active').parent().prev('li').children().addClass('active').attr("aria-selected","true");
    //     $(activeTab).removeClass('active').prev().addClass('active');
    // });
    $('.find-page').on('click', '#filter .close', function (e) {
        e.preventDefault();
        let clear = true;
        let tab = $(e.target).parent().attr('data-tab');
        let id = $(e.target).parent().attr('data-id');
        let parentTabPane = $('.tab-pane.active').attr('id');
        let data = {
          tab: tab,
          id: id,
          ParentTab: parentTabPane,
          clear: clear
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=UTF-8",
            url: url,
            data: JSON.stringify(data),
            beforeSend: function() {
                $('#floatingBarsG').show();
            },
            complete: function() {
                $('#floatingBarsG').hide();
            },
            success: function (data) {
                if(data.tab == 'specspeciality') {
                    $('#specspeciality table tbody').html(data.html);
                }
                else if(data.tab == 'specfullname') {
                    $('#specfullname table tbody').html(data.html);
                }
                else if(data.tab == 'ticketdate'){
                    $('#ticketdate table tbody').html(data.html);
                }
                else if(data.tab == 'tickettime'){
                    $('#tickettime table tbody').html(data.html);
                }
                else if(data.tab == 'final'){
                    $('#zapis table tbody').html(data.html);
                    // $('#form').html(data.htmlForm);
                }else{
                    throw Error;
                }
            },
            error: function () {
                // Показываем ошибку
                $('.alert').show();
            }
        })

    });
    $('.tab-content').on('click', '.choice', function (e) {
        e.preventDefault();
        $('.readmore,.readmoreBack').hide();
        let parentTabPane = $(this).closest('.tab-pane.active').attr('id');
        let choiceText = $(this).text();
        let id = $(this).attr('id');
        let name = $(this).text();
        let activeTab = $('.tab-pane.active');
        let currentLink = $('.nav-link.active');
        let arr = [];
        let scroll = $(window).scrollTop();
        $('#filter div').each(function () {
            arr.push($(this));
        });
        $.each(arr, function (id, val) {
            if(activeTab.attr('id') == val.attr('data-tab')) {
                $(this).nextAll().remove();
                $(this).remove();
            }
        });
        let data = {
            'ParentTab': parentTabPane,
            'id': id,
            'name': name
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=UTF-8",
            url: url,
            data: JSON.stringify(data),
            beforeSend: function() {
                $('#floatingBarsG').show();
            },
            complete: function() {
                $('#floatingBarsG').hide();
            },
            success: function (data) {
                $('.alert.alert-danger').hide();
                // Переключаем вкладки
                let nextLink = $(currentLink).attr("aria-selected","false").removeClass('active').parent().next('li').children();
                nextLink.addClass('active').attr("aria-selected","true");
                $(activeTab).removeClass('active').next().addClass('active');
                // Добавляем выбранный элемент в фильтр и массив
                let choiceHtml = '<div class="alert alert-success" role="alert">' + choiceText + '<button class="close" data-dismiss="alert">×</button></div>';
                if (items.indexOf(choiceText) == -1) items.push(choiceText);
                // Отрисовываем элемент в диве фильтра
                $('#filter').append(choiceHtml);
                $('#filter div:last-child').attr('data-id', id).attr('data-tab', parentTabPane);
                // $('#filter div:last-child').attr('data-tab', parentTabPane);
                if(data.tab == 'specspeciality') {
                    $('#specspeciality table tbody').html(data.html);
                    $('#specfullname table tbody').html(data.employeesHtml);
                    // $('.breadcrumbs').html(data.breadCrumbsHtml);
                    $(window).scrollTop(scroll);
                    // window.history.pushState({},"", location.pathname.substr(1)+"#"+data.tab);
                }
                else if(data.tab == 'specfullname') {
                    $('#specfullname table tbody').html(data.html);
                    $('#ticketdate table tbody').html(data.datesHTML);
                    // window.history.pushState({},"", location.pathname.substr(1)+"#"+data.tab);
                    $(window).scrollTop(scroll);
                }
                else if(data.tab == 'ticketdate') {
                    $('#ticketdate table tbody').html(data.html);
                    // window.history.pushState({},"", location.pathname.substr(1)+"#"+data.tab);
                    $(window).scrollTop(scroll);
                }
                else if(data.tab == 'tickettime') {
                    $('#tickettime table tbody').html(data.html);
                    $('#filter .close').prop('disabled', true);
                    // window.history.pushState({},"", location.pathname.substr(1)+"#"+data.tab);
                    $(window).scrollTop(scroll);
                }
                else if(data.tab == 'final') {
                    $('#zapis table tbody').html(data.html);
                    $('#clockdiv').show();
                    countDown(data.time);
                    // window.history.pushState({},"", location.pathname.substr(1)+"#"+data.tab);
                    $(window).scrollTop(scroll);
                    // $('#form').html(data.htmlForm);
                }else{
                    throw Error;
                }
            },
            error: function () {
                // Показываем ошибку
                $('.alert').show();
            }
        })
    });

    $('.newdates').on('keyup', function (e) {
        e.preventDefault();
        if(e.keyCode === 13) {
            let activeTab = $('.tab-pane.active').attr('id');
            let StartDate = $('input[name=StartDate]').val().split('-').reverse().join('-');
            let EndDate = $('input[name=EndDate]').val().split('-').reverse().join('-');
            if(Date.parse(StartDate) > Date.parse(EndDate)) $('input[name=StartDate]').val($('input[name=EndDate]').val());
            else {
                let data = {
                    StartDate: StartDate,
                    EndDate: EndDate,
                    dataTab: activeTab
                };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=UTF-8",
                    url: datesUrl,
                    data: JSON.stringify(data),
                    error: function () {
                        // Показываем ошибку
                        $('.alert').show();
                    },
                    success: function (data) {
                        $('#ticketorganization table tbody').html(data.appointments);
                        $('#specspeciality table tbody').html(data.specialities);
                        $('#specfullname table tbody').html(data.employees);
                        // $('#ticketdate table tbody').html(data.dates);
                    }
                });
            }
        }
    });
    $('body').on('submit', 'form', function (e) {
        e.preventDefault();
        let url = $(this).attr('action');
        let data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data) {
                $('form').trigger("reset");
                $('#loginpopup').show(600);
                setTimeout(window.location.href = data.redirect, 1500);
            },
            error: function (data) {
                let errors = data.responseJSON;
                let errorsHtml = '';
                $.each(errors, function (key, value) {
                    errorsHtml += '<div class="alert alert-danger error" role="alert">' + value[0] + '<button type="button" class="close" data-dismiss="alert">×</button></div>';
                });
                $('#form-errors').html(errorsHtml);
            }
        });
    });
    $('#resetTime').click(function() {
        let reset = true;
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=UTF-8",
            url: timeUrl,
            data: {reset: reset},
            success: function (data) {

            },
            error: function () {
                // Показываем ошибку
                console.log('Some bloody error');
            }
        });
    });
    // $("body").mouseover(function(){
    //     out=false;
    // }).mouseout(function(){
    //     out=true;
    // });
    // $(window).bind('beforeunload', function(e){
    //     e.preventDefault();
    //     if(out) {
    //         closeWindow();
    //     }
    // });
    $('input[name="fio"]').on('keyup', function() {
        let nameArr =  $(this).val().split(' ');
        $('input[name="surname"]').val(nameArr[0]);
        $('input[name="name"]').val(nameArr[1]);
        $('input[name="thirdname"]').val(nameArr[2]);
    });
    $('.fio').on('keyup', function(e) {
        e.preventDefault();
        let str = '';
        $('.fio').each(function () {
            str += $(this).val() + ' ';
        });
        $('input[name="fio"]').val(str);
    });
});