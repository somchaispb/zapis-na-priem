let deleteBtn = $('a[href^="/delete"]');
let deleteBulkBtn = $('a[data-id="delete-bulk"]');
let cancelBulkBtn = $('a[data-id="cancel-ticket"]');
let token = $('meta[name=csrf-token]').attr("content");
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': token
    },
});
$(function () {
    $(deleteBtn).click(function (e) {
        e.preventDefault();
        let urlArr =  deleteBtn.context.URL.split('/');
        let id = urlArr[urlArr.length - 1];
        let url = '/ticket/delete/' + id;
        if (!confirm("Вы уверены, что хотите удалить номерок?")) return;
        $.ajax({
            type: "DELETE",
            url: url,
            data: {"action": "delete"},
            success: function (data) {
                if (data.status === 'deleted') {
                    $('#loginpopup').show(500);
                    setTimeout(function () {
                        window.location.replace('/');
                    }, 1000);
                }
            },
            error: function (data) {
                let errors = data.responseJSON;
                let errorsHtml = '';
                $.each(errors, function (key, value) {
                    errorsHtml += '<div class="alert alert-danger error" role="alert">' + value[0] + '<button type="button" class="close" data-dismiss="alert">×</button></div>';
                });
                $('#form-errors').html(errorsHtml);
            }
        });

    });
    $(deleteBulkBtn).click(function (e) {
        e.preventDefault();
        let url = '/ticket/delete/bulk';
        let ids = [];
        $('.checkbox :checked').each(function () {
           ids.push($(this).val());
        });
        if (!confirm("Вы уверены, что хотите удалить номерки?")) return;
        $.ajax({
            type: "POST",
            url: url,
            data: {"action": "delete-bulk", "id": ids},
            success: function (data) {
                if (data.status === 'deleted') {
                    $('#loginpopup').show(500);
                    setTimeout(function () {
                        window.location.replace('/');
                    }, 1000);
                }
            },
            error: function (data) {
                let errors = data.responseJSON;
                let errorsHtml = '';
                $.each(errors, function (key, value) {
                    errorsHtml += '<div class="alert alert-danger error" role="alert">' + value[0] + '<button type="button" class="close" data-dismiss="alert">×</button></div>';
                });
                $('#form-errors').html(errorsHtml);
            }
        });
    })
    $(cancelBulkBtn).click(function (e) {
        e.preventDefault();
        let url = '/ticket/cancel';
        let ids = [];
        $('.checkbox :checked').each(function () {
            ids.push($(this).val());
        });
        if (!confirm("Вы уверены, что хотите отменить запись?")) return;
        $.ajax({
            type: "POST",
            url: url,
            data: {"action": "cancel", "id": ids},
            success: function (data) {
                if (data.status === 'canceled') {
                    $('#loginpopup').show(500);
                    setTimeout(function () {
                        $('.checkbox :checked').each(function () {
                            $(this).prop('checked', false);
                        });
                        location.reload();
                    }, 1000);
                }
            },
            error: function (data) {
                let errors = data.responseJSON;
                let errorsHtml = '';
                $.each(errors, function (key, value) {
                    errorsHtml += '<div class="alert alert-danger error" role="alert">' + value[0] + '<button type="button" class="close" data-dismiss="alert">×</button></div>';
                });
                $('#form-errors').html(errorsHtml);
            }
        });
    })
});