let page = 1;

// $(window).scroll(function() {
//     if($(window).scrollTop() + $(window).height() >= $(document).height()) {
//         page++;
//         loadMoreData(page);
//     }
// });

$('.readmore').click(function () {
    page++;
    loadMoreData(page);
});

$('.readmoreBack').click(function () {
    if(page > 1) page--;
    loadMoreData(page);
});

function loadMoreData(page) {
    $.ajax({
        url: '?page=' + page,
        type: "get",
        beforeSend: function () {
            $('#floatingBarsG').show();
        },
        complete: function () {
            $('#floatingBarsG').hide();
        },
        success: function (data) {
            if (data == "") {
                $('#specfullname table tbody').html("<tr><td class='text-center' colspan='5'>Больше результатов нет</td></tr>");
                return;
            }
            // $("#specialist tbody:last-child").append(data); // добавление к существующим записям
            $("#specfullname table tbody").html(data); // обновление таблицы
        },
        error: function (jqXHR, ajaxOptions, thrownError) {
            // Показываем ошибку
            console.log(jqXHR, ajaxOptions, thrownError);
        }
    });
}