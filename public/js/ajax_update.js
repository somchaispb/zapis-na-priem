$(function () {
    $('input[type=checkbox]').click(function () {
        if($(this).is(':checked')) $('.submit').prop('disabled', false);
        else $('.submit').prop('disabled', true);
    });
    $('body').on('submit', 'form', function (e) {
        e.preventDefault();
        let url = '/updateticket';
        let data = $(this).serialize();
        // let
        $.ajax({
            type: "PUT",
            url: url,
            data: data,
            success: function (data) {
                if (data.status === 'updated') alert ('Номерок успешно обновлен');
                $('form').trigger("reset");
            },
            error: function (data) {
                let errors = data.responseJSON;
                let errorsHtml = '';
                $.each(errors, function (key, value) {
                    errorsHtml += '<div class="alert alert-danger error" role="alert">' + value[0] + '<button type="button" class="close" data-dismiss="alert">×</button></div>';
                });
                $('#form-errors').html(errorsHtml);
            }
        });

    })
});