<!DOCTYPE html>
<html lang="ru">
<head>
    @include('inc.head')
</head>
<body>

@include('inc.nav')

@if(!empty($errors))
    @include('inc.messages')
@endif

@yield('content')

@include('inc.footer')
@yield('ajaxForm')
@yield('ajaxUpdate')
@yield('ajaxDelete')

</body>
</html>