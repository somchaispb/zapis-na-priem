@extends('layouts.app')
@section('title', 'Список номерков')
@section('content')
    <div class="container">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <button type="button" class="btn btn-outline-secondary">Действия</button>
                <button type="button" class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <div class="dropdown-menu">
                    <a data-id="delete-bulk" class="dropdown-item">Удалить</a>
                    <a data-id="cancel-ticket" class="dropdown-item">Отменить запись</a>
                    <div role="separator" class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Separated link</a>
                </div>
            </div>
        </div>
        <div class="container">
            @foreach($tickets as $ticket)
                <div class="row">
                    <div class="col-md-1 checkbox">
                        <input name="appointmentticketid[]" type="checkbox" value="{{$ticket->appointmentticketid}}">
                    </div>
                    <div class="col-md-3">{{$ticket->specfullname}}</div>
                    <div class="col-md-2">{{$ticket->patfullname}}</div>
                    <div class="col-md-1">{{$ticket->ticketdate}}</div>
                    <div class="col-md-1">{{$ticket->tickettime}}</div>
                    <div class="col-md-1">
                        <a href="{{action('TicketController@update', $ticket->appointmentticketid)}}">Редактировать</a>
                    </div>
                    <div class="col-md-1">
                        <a href="{{action('TicketController@edit', $ticket->appointmentticketid)}}">Изменить</a>
                    </div>
                    <div class="col-md-1">
                        <a data-id="delete" href="{{action('TicketController@delete', $ticket->appointmentticketid)}}" >Удалить</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div id="loginpopup" class="modal fade show" role="dialog" style="padding-right: 17px; display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5>Номерки успешно удалены</h5>
                </div>
                <div class="modal-body text-center">
                    <div class="row">
                        <div class="col-md-12">
                            Вы будете перенаправлены на главную страницу
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('ajaxDelete')
    <script src="{{asset('js/ajax_delete.js')}}"></script>
@endsection