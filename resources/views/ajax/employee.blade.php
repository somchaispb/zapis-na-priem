@if(!empty($employees))
    @php $i = 1; @endphp
    @foreach($employees as $employee)
        <tr>
            {{--            <td><input type="checkbox" name="multi" value="{{$employee->specfullname}}"></td>--}}
            <td>{{$i}}</td>
            <td id="{{$employee->specfullname}}" class="mdl-data-table__cell--non-numeric choice">{{$employee->specfullname}} ({{$employee->name}})</td>
            <td class="mdl-data-table__cell--non-numeric"><span class="font-weight-bold">{{$employee->freeticket}}</span> из {{$employee->freeticket+$employee->bookedticket}}</td>
            <td>{{mt_rand(0, 1000)}}</td>
            <td class="mdl-data-table__cell--non-numeric"><a href="#">www.test.loc</a></td>
        </tr>
        @php $i++; @endphp
    @endforeach
@endif