@if(!empty($times))
    @php $i = 1; @endphp
    @foreach($times as $time)
        <tr>
            <td>{{$i}}</td>
            <td id="{{$time->tickettime}}" class="mdl-data-table__cell--non-numeric
            @if($time->ticketstatus == 1)
            choice
            @else
            not-active
            @endif
            ">
                {{substr($time->tickettime, 0, 5)}}
            </td>
                @if($time->ticketstatus == 1)
                <td class="mdl-data-table__cell--non-numeric"><span class="badge badge-success">свободен</span></td>
                @elseif($time->ticketstatus == 3)
                <td class="mdl-data-table__cell--non-numeric"><span class="badge badge-danger">заблокирован</span></td>
                @else
                <td class="mdl-data-table__cell--non-numeric"><span class="badge badge-danger">занят</span></td>
                @endif
        </tr>
        @php $i++; @endphp
    @endforeach
@endif