<form action="{{action('PagesController@saveForm')}}">
    {{csrf_field()}}
    <div id="form-errors"></div>
    <div class="form-group">
        <label for="surname">Фамилия</label>
        <input required class="form-control" type="text" name="surname" value="{{ old('surname') }}">
    </div>
    <div class="form-group">
        <label for="name">Имя</label>
        <input required class="form-control" type="text" name="name" value="{{ old('name') }}">
    </div>
    <div class="form-group">
        <label for="thirdname">Отчество</label>
        <input class="form-control" type="text" name="thirdname" value="{{ old('thirdname') }}">
    </div>
    <div class="form-group">
        <label for="dob">Дата рождения</label>
        <input required class="form-control" type="text" name="dob" value="{{ old('dob') }}">
    </div>
    <div class="form-group">
        <label for="policenumber">Номер полиса</label>
        <input  class="form-control" type="text" name="policenumber" value="{{ old('policenumber') }}">
    </div>
    <div class="form-group">
        <label for="policeexpire">Дата действия полиса</label>
        <input  class="form-control" type="text" name="policeexpires" value="{{ old('policeexpires') }}">
    </div>
    <div class="form-group">
        <label for="tel">Телефон пациента</label>
        <input required class="form-control" type="tel" name="tel" value="{{ old('tel') }}">
    </div>
    <div class="form-group">
        <label for="email">E-mail пациента</label>
        <input required class="form-control" type="email" name="email" value="{{ old('email') }}">
    </div>
    <div class="form-group">
        <label for="purpose">Цель посещения</label>
        <input class="form-control" type="text" name="purpose" value="{{ old('purpose') }}">
    </div>
    <div class="form-group">
        <label for="extra">Примечание</label>
        <textarea class="form-control" rows="10" cols="45" name="extra"></textarea>
    </div>
    <button class="btn btn-info btn-lg m-1" type="submit">Отправить</button>
    <div class="postData"></div>
</form>