@if($dates->count() > 0)
    @php $k=1; @endphp
    @foreach($dates as $date)
        <tr>
            <td>{{$k}}</td>
            <td id="{{$date->ticketdate}}" class="mdl-data-table__cell--non-numeric choice">{{\Carbon\Carbon::parse($date->ticketdate)->formatLocalized('%d %B %Y. %A')}}</td>
            <td class="mdl-data-table__cell--non-numeric"><span class="font-weight-bold">{{$date->freeticket}}</span> из {{$date->freeticket+$date->bookedticket}}</td>
            <td class="mdl-data-table__cell--non-numeric">с {{substr($date->min_time,0, 5)}} до {{substr($date->max_time, 0, 5)}}</td>
        </tr>
        @php $k++; @endphp
    @endforeach
@else
    <tr>
        <td class="text-center" colspan="4">
            Нет дат
        </td>
    </tr>
@endif

