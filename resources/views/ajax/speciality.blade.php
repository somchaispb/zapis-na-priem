
@if(!empty($specialities))
@php $j=1; @endphp
@foreach($specialities as $speciality)
    <tr>
        <td>{{$j}}</td>
        <td id="{{$speciality->specspeciality}}" class="mdl-data-table__cell--non-numeric choice">{{$speciality->name}} ({{$speciality->orgname}})</td>
        <td class="mdl-data-table__cell--non-numeric"><span class="font-weight-bold">{{$speciality->freeticket}}</span> из {{$speciality->freeticket+$speciality->bookedticket}}</td>
    </tr>
    @php $j++; @endphp
@endforeach
@endif
