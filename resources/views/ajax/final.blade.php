@if(!empty($final))
    @foreach($final as $item)
        <tr>
        <td>{{$item->appointmentticketid}}</td>
        <td id="visitstatus">свободен</td>
            <td id="visitspeciality"><span class="font-weight-bold">{{$item->d_speciality->name}}</span>></td>
        <td id="visitspecialist">{{$item->specfullname}}</td>
        <td id="visitdate"><span class="font-weight-bold">{{\Carbon\Carbon::parse($item->ticketdate)->formatLocalized('%d %B %Y. %A')}}</span></td>
        <td id="visittime"><span class="badge badge-danger">{{substr($item->tickettime, 0, 5)}}</span></td>
    </tr>
    @endforeach
@endif