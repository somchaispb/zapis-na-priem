@if(!empty($appointments))
    @php $j=1; @endphp
    @foreach($appointments as $appointment)
        @php
            $orgname = explode(',', $appointment->orgname);
        @endphp
        <tr>
            {{--<td><input type="checkbox" value="{{$appointment->ticketorganization}}"></td>--}}
            <td>{{$j}}</td>
            <td id="{{$appointment->ticketorganization}}" class="mdl-data-table__cell--non-numeric choice">{{$orgname[0]}}</td>
            <td class="mdl-data-table__cell--non-numeric">{{$appointment->spec_qty}} / {{$appointment->empl_qty}}</td>
            <td class="mdl-data-table__cell--non-numeric"><span class="font-weight-bold">{{$appointment->freeticket}}</span> из {{$appointment->freeticket+$appointment->bookedticket}}</td>
            <td class="mdl-data-table__cell--non-numeric">{{mt_rand(0, 1000)}}</td>
            <td><a href="#">www.test.loc</a></td>
        </tr>
        @php $j++; @endphp
    @endforeach
@endif