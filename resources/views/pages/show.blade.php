@extends('layouts.app')
@section('title', 'Номерок в '. $ticket->d_organization->name)
@section('content')
    {{--<ul class="breadcrumbs">--}}
    {{--@foreach ($breadcrumbs as $key => $url)--}}
        {{--<li class="{{ $loop->last ? 'is-active' : '' }}">--}}
            {{--@if (! $loop->last)--}}
                {{--<a href="{{ url($url) }}">--}}
                {{--{{ ucfirst($key) }}--}}
                    {{--->--}}
            {{--@else--}}
                {{--@yield ('title')--}}
            {{--@endif--}}
            {{--</a>--}}
        {{--</li>--}}
    {{--@endforeach--}}
    {{--</ul>--}}
    <!-- Start My Appointment Section -->
    <div id="myappointment-page" class="layer-stretch">
        <div class="layer-wrapper layer-bottom-0">
            <div class="row myappointment-container">
                <div class="col-md-12">
                        <div class="theme-material-card text-center">
                            <div class="myappointment-text">
                                <i class="fa fa-hospital-o"></i>
                                <span class="paragraph-medium paragraph-black">Клиника - {{$ticket->d_organization->name}}</span>
                            </div>
                            <div class="myappointment-text">
                                <i class="fa fa-id-card"></i>
                                <span class="paragraph-medium paragraph-black">ID - {{$ticket->appointmentticketid}}</span>
                            </div>
                            <div class="myappointment-text">
                                <i class="fa fa-user-md"></i>
                                <span class="paragraph-medium paragraph-black"><a href="#">{{$ticket->specfullname}}</a></span>
                            </div>
                            <div class="myappointment-text">
                                <i class="fa fa-user-o"></i>
                                <span class="paragraph-medium paragraph-black">{{$ticket->patfullname}}</span>
                            </div>
                            <div class="myappointment-text">
                                <i class="fa fa-calendar-plus-o"></i>
                                <span class="paragraph-medium paragraph-black">{{$ticket->ticketdate}}</span>
                            </div>
                            <div class="myappointment-text">
                                <i class="fa fa-clock-o"></i>
                                <span class="paragraph-medium paragraph-black">{{$ticket->tickettime}}</span>
                            </div>
                            <div class="myappointment-text">
                                <i class="fa fa-flag-o"></i>
                                <span class="paragraph-medium paragraph-black">
                                    @if($ticket->ticketstatus == 2)
                                        Забронировано
                                    @else
                                        В ожидании
                                    @endif
                                </span>
                            </div>
                            <div class="myappointment-view">
                                <a href="/update/{{$ticket->appointmentticketid}}" class="mdl-button mdl-js-button mdl-button--colored mdl-js-ripple-effect mdl-button--raised button button-primary button-sm pull-left">Изменить</a>
                                <a data-id="delete" href="/delete/{{$ticket->appointmentticketid}}" class="mdl-button mdl-js-button mdl-button--colored mdl-js-ripple-effect mdl-button--raised button button-primary button-sm pull-right">Удалить</a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div><!-- End My Appointment Section -->
    <div id="loginpopup" class="modal fade show" role="dialog" style="padding-right: 17px; display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5>Номерок успешно удален</h5>
                </div>
                <div class="modal-body text-center">
                    <div class="row">
                        <div class="col-md-12">
                            Вы будете перенаправлены на страницу списка всех номерков.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('ajaxDelete')
    <script src="{{asset('js/ajax_delete.js')}}"></script>
@endsection