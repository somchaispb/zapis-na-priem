@extends('layouts.app')
@section('title', 'Запись на прием')
@section('content')
    {{--<ul class="breadcrumbs">--}}
        {{--@foreach ($breadcrumbs as $key => $url)--}}
            {{--<li class="{{ $loop->last ? 'is-active' : '' }}">--}}
                {{--@if (! $loop->last)--}}
                    {{--<a href="{{ url($url) }}">--}}
                        {{--{{ ucfirst($key) }}--}}
                        {{--->--}}
                        {{--@else--}}
                            {{--@yield ('title')--}}
                        {{--@endif--}}
                    {{--</a>--}}
            {{--</li>--}}
        {{--@endforeach--}}
    {{--</ul>--}}
    <!-- Start find page -->
    <div class="find-page container">
        <div class="row">
            <div class="col-md-2">
                <h2>Вы выбрали:</h2>
                <div id="filter">

                </div>
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Форма поиска номерков</h2>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row newdates">
                                    <div class="form-group col-sm-6">
                                        <label for="StartDate">Дата с</label>
                                        <input placeholder="дд.мм.гггг" required class="form-control" type="text" name="StartDate" value="{{  $dates[0]  }}">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="EndDate">по</label>
                                        <input placeholder="дд.мм.гггг" required class="form-control" type="text" name="EndDate" value="{{ $dates[1] }}">
                                    </div>
                                </div>
                                {{--<p class="text-center">--}}
                                    {{--<button class="new-dates button-icon button-iconfill-purple m-1"><span>Обновить даты</span></button>--}}
                                {{--</p>--}}
                            </div>
                            {{--<div class="col-md-4">--}}
                                {{--<div id="clockdiv" style="display: none;">--}}
                                    {{--<p>Ваша сессия закончится через: </p>--}}
                                    {{--<div>--}}
                                        {{--<span class="minutes"></span>--}}
                                        {{--<div class="smalltext">Минут</div>--}}
                                    {{--</div>--}}
                                    {{--<div>--}}
                                        {{--<span class="seconds"></span>--}}
                                        {{--<div class="smalltext">Секунд</div>--}}
                                    {{--</div>--}}
                                    {{--<button class="btn btn-light" id="resetTime">Сбросить</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="breadcrumbs">

                                </div>
                            </div>
                        </div>
                        <div class="alert alert-danger" role="alert" style="display: none;">
                            <strong>Ой!</strong> Что-то пошло не так, попробуйте обновить страницу
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" href="#ticketorganization" role="tab" data-toggle="tab" aria-selected="true">Клиника</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#specspeciality" role="tab" data-toggle="tab" aria-selected="false">Специальность/услуга</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#specfullname" role="tab" data-toggle="tab" aria-selected="false">Специалист</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#ticketdate" class="nav-link" role="tab" data-toggle="tab" aria-selected="false">Дата</a>
                            </li>
                            <li class="nav-item">
                                <button disabled class="nav-link" role="tab" data-toggle="tab" aria-selected="false">Время</button>
                            </li>
                            <li class="nav-item">
                                <button disabled class="nav-link" href="#zapis" role="tab" data-toggle="tab" aria-selected="false">Запись</button>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="ticketorganization">
                                <h5>Клиника</h5>
                                <!-- Таблица -->
                                <div class="theme-material-card">
                                    <table class="table" data-upgraded=",MaterialDataTable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="mdl-data-table__cell--non-numeric">Клиника</th>
                                            <th class="mdl-data-table__cell--non-numeric">Врачей / Специальностей</th>
                                            <th class="mdl-data-table__cell--non-numeric">Номерков</th>
                                            <th class="mdl-data-table__cell--non-numeric">Отзывы</th>
                                            <th>Информационная страница</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($appointments))
                                            @php $i = 1; @endphp
                                            @foreach($appointments as $appointment)
                                                @php
                                                    $orgname = explode(',', $appointment->orgname);
                                                @endphp
                                                <tr>
                                                    {{--<td><input type="checkbox" name="multi" value="{{$appointment->ticketorganization}}"></td>--}}
                                                    <td>{{$i}}</td>
                                                    <td id="{{$appointment->ticketorganization}}" class="mdl-data-table__cell--non-numeric choice">{{$orgname[0]}}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">{{$appointment->spec_qty}} / {{$appointment->empl_qty}}</td>
                                                    <td class="mdl-data-table__cell--non-numeric"><span class="font-weight-bold">{{$appointment->freeticket}}</span> из {{$appointment->freeticket+$appointment->bookedticket}}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">{{mt_rand(0, 1000)}}</td>
                                                    <td><a href="#">www.test.loc</a></td>
                                                </tr>
                                                @php $i++; @endphp
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- Таблица конец -->
                                {{--<div class="p2 text-center">--}}
                                    {{--<button class="button-icon button-iconfill-purple m-1">--}}
                                        {{--<span>Подобрать</span>--}}
                                    {{--</button>--}}
                                    {{--<button class="button-icon forward button-icon-purple m-1">--}}
                                        {{--<span>Дальше</span><i class="fa fa-arrow-right"></i>--}}
                                    {{--</button>--}}
                                {{--</div>--}}
                            </div>
                            <div role="tabpanel" class="tab-pane" id="specspeciality">
                                <div class="p-2">
                                    <h5>Специальность/услуга</h5>
                                    <!-- Таблица -->
                                    <div class="theme-material-card">
                                        <table class="table" data-upgraded=",MaterialDataTable">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class="mdl-data-table__cell--non-numeric">Специализация</th>
                                                <th class="mdl-data-table__cell--non-numeric">Номерков</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($specialities))
                                                @php $j=1; @endphp
                                                @foreach($specialities as $speciality)
                                                    <tr>
                                                        <td>{{$j}}</td>
                                                        <td id="{{$speciality->specspeciality}}" class="mdl-data-table__cell--non-numeric choice">{{$speciality->name}} ({{$speciality->orgname}})</td>
                                                        <td class="mdl-data-table__cell--non-numeric">{{$speciality->freeticket}} из {{$speciality->freeticket+$speciality->bookedticket}}</td>
                                                    </tr>
                                                    @php $j++; @endphp
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- Таблица конец -->
                                    {{--<div class="p2 text-center">--}}
                                        {{--<button class="button-icon back button-icon-purple m-1">--}}
                                            {{--<i class="fa fa-arrow-left"></i> <span>Назад</span>--}}
                                        {{--</button>--}}
                                        {{--<button class="button-icon button-iconfill-purple m-1">--}}
                                            {{--<span>Подобрать</span>--}}
                                        {{--</button>--}}
                                        {{--<button class="button-icon forward button-icon-purple m-1">--}}
                                            {{--<span>Дальше</span><i class="fa fa-arrow-right"></i>--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="specfullname">
                                <div class="p-2">
                                    <h5>Специалист</h5>
                                    <!-- Таблица -->
                                    <div class="theme-material-card">
                                        <table class="table" data-upgraded=",MaterialDataTable">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class="mdl-data-table__cell--non-numeric">Специалист</th>
                                                <th class="mdl-data-table__cell--non-numeric">Номерков</th>
                                                <th class="mdl-data-table__cell--non-numeric">Отзывы</th>
                                                <th class="mdl-data-table__cell--non-numeric">О специалисте</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($employees))
                                                @php
                                                $j = 1;
                                                @endphp
                                                @foreach($employees as $employee)
                                                    <tr>
                                                        {{--<td><input type="checkbox" name="multi" value="{{$employee->specfullname}}"></td>--}}
                                                        <td>{{$j}}</td>
                                                        <td id="{{$employee->specfullname}}" class="mdl-data-table__cell--non-numeric choice">{{$employee->specfullname}} ({{$employee->name}})</td>
                                                        <td class="mdl-data-table__cell--non-numeric">{{$employee->freeticket}} из {{$employee->freeticket+$employee->bookedticket}}</td>
                                                        <td>{{mt_rand(0, 1000)}}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><a href="#">www.test.loc</a></td>
                                                    </tr>
                                                    @php
                                                        $j++;
                                                    @endphp
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                        @if(count($employees) > 20)
                                            <div class="text-center">
                                                <button class="btn btn-success readmoreBack">Назад</button>
                                                <button class="btn btn-success readmore">Вперед</button>
                                            </div>
                                        @endif
                                    </div>
                                    <!-- Таблица конец -->
                                    {{--<div class="p2 text-center">--}}
                                        {{--<button class="button-icon back button-icon-purple m-1">--}}
                                            {{--<i class="fa fa-arrow-left"></i> <span>Назад</span>--}}
                                        {{--</button>--}}
                                        {{--<button class="button-icon button-iconfill-purple m-1">--}}
                                            {{--<span>Подобрать</span>--}}
                                        {{--</button>--}}
                                        {{--<button class="button-icon forward button-icon-purple m-1">--}}
                                            {{--<span>Дальше</span><i class="fa fa-arrow-right"></i>--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="ticketdate">
                                <div class="p-2">
                                    <h5>Дата</h5>
                                    <!-- Таблица -->
                                    <div class="theme-material-card">
                                        <table class="table" data-upgraded=",MaterialDataTable">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class="mdl-data-table__cell--non-numeric">Дата</th>
                                                <th class="mdl-data-table__cell--non-numeric">Номерков</th>
                                                <th class="mdl-data-table__cell--non-numeric">Период</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {{--@if(!empty($dates))--}}
                                            {{--@php $k=1; @endphp--}}
                                            {{--@foreach($dates as $date)--}}
                                            {{--<tr>--}}
                                            {{--<td>{{$k}}</td>--}}
                                            {{--<td class="mdl-data-table__cell--non-numeric choice">{{\Carbon\Carbon::parse($date->ticketdate)->format('d.m.Y')}}</td>--}}
                                            {{--<td class="mdl-data-table__cell--non-numeric">{{$date->freeticket}} из {{$date->freeticket+$date->bookedticket}}</td>--}}
                                            {{--<td class="mdl-data-table__cell--non-numeric">с {{substr($date->min_Time,0, 5)}} до {{substr($date->max_Time, 0, 5)}}</td>--}}
                                            {{--</tr>--}}
                                            {{--@php $k++; @endphp--}}
                                            {{--@endforeach--}}
                                            {{--@endif--}}
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- Таблица конец -->
                                    {{--<div class="p2 text-center">--}}
                                        {{--<button class="button-icon back button-icon-purple m-1">--}}
                                            {{--<i class="fa fa-arrow-left"></i> <span>Назад</span>--}}
                                        {{--</button>--}}
                                        {{--<button class="button-icon button-iconfill-purple m-1">--}}
                                            {{--<span>Подобрать</span>--}}
                                        {{--</button>--}}
                                        {{--<button class="button-icon forward button-icon-purple m-1">--}}
                                            {{--<span>Дальше</span><i class="fa fa-arrow-right"></i>--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="tickettime">
                                <div class="p-2">
                                    <h5>Время</h5>
                                    <!-- Таблица -->
                                    <div class="theme-material-card">
                                        <table class="table" data-upgraded=",MaterialDataTable">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class="mdl-data-table__cell--non-numeric">Время</th>
                                                <th class="mdl-data-table__cell--non-numeric">Статус</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- Таблица конец -->
                                    {{--<div class="p2 text-center">--}}
                                        {{--<button class="button-icon back button-icon-purple m-1">--}}
                                            {{--<i class="fa fa-arrow-left"></i> <span>Назад</span>--}}
                                        {{--</button>--}}
                                        {{--<button class="button-icon button-iconfill-purple m-1">--}}
                                            {{--<span>Подобрать</span>--}}
                                        {{--</button>--}}
                                        {{--<button class="button-icon forward button-icon-purple m-1">--}}
                                            {{--<span>Дальше</span><i class="fa fa-arrow-right"></i>--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="zapis">
                                <div class="container">
                                    <div class="sub-ttl">Информация о записи</div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Таблица -->
                                            <table class="table table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Номерок №</th>
                                                    <th>Статус</th>
                                                    <th>Специализация</th>
                                                    <th>Специалист</th>
                                                    <th>Дата</th>
                                                    <th>Время</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                            <!-- Таблица конец -->
                                        </div>
                                    </div>
                                    <div class="sub-ttl">Данные о пациенте</div>
                                    <div class="row">
                                        <div id="form" class="col-md-10">
                                            <form action="{{Session::has('oldTicket') ? action('TicketController@editForm', Session::get('oldTicket')->appointmentticketid) : action('AjaxController@saveForm')}}" method="POST">
                                                {{csrf_field()}}
                                                <div id="form-errors"></div>
                                                <div class="form-group">
                                                    <label for="surname">Фамилия</label>
                                                    <input class="form-control fio" type="text" name="surname" value="{{ Session::has('oldTicket') ? Session::get('oldTicket')->patlastname : old('surname') }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="name">Имя</label>
                                                    <input class="form-control fio" type="text" name="name" value="{{ Session::has('oldTicket') ? Session::get('oldTicket')->patfirstname : old('name') }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="thirdname">Отчество</label>
                                                    <input class="form-control fio" type="text" name="thirdname" value="{{ Session::has('oldTicket') ? Session::get('oldTicket')->patpatronymicname : old('thirdname') }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="fio">ФИО</label>
                                                    <input class="form-control" type="text" name="fio" value="{{ Session::has('oldTicket') ? Session::get('oldTicket')->patfullname : old('fio') }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="dob">Дата рождения</label>
                                                    <input placeholder="дд.мм.гггг" class="form-control" type="text" name="dob" value="{{ Session::has('oldTicket') ? Session::get('oldTicket')->patbirthdate : old('dob') }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="policenumber">Номер полиса</label>
                                                    <input  class="form-control" type="text" name="policenumber" value="{{ Session::has('oldTicket') ? Session::get('oldTicket')->patpolicynum : old('policenumber') }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="policeexpire">Дата действия полиса</label>
                                                    <input placeholder="дд.мм.гггг"  class="form-control" type="text" name="policeexpires" value="{{ Session::has('oldTicket') ? Session::get('oldTicket')->patpolicydate : old('policeexpires') }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="tel">Телефон пациента</label>
                                                    <input placeholder="8(911)123-12-12" required class="form-control" type="tel" name="tel" value="{{ Session::has('oldTicket') ? Session::get('oldTicket')->patphone : old('tel') }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">E-mail пациента</label>
                                                    <input class="form-control" type="email" name="email" value="{{ Session::has('oldTicket') ? Session::get('oldTicket')->patemail : old('email') }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="purpose">Цель посещения</label>
                                                    <input class="form-control" type="text" name="purpose" value="{{ Session::has('oldTicket') ? Session::get('oldTicket')->patvisitpurpose : old('purpose') }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="extra">Примечание</label>
                                                    <textarea class="form-control" rows="10" cols="45" name="extra">{{Session::has('oldTicket') ? Session::get('oldTicket')->patcomment : ''}}</textarea>
                                                </div>
                                                <button class="btn btn-info btn-lg m-1 send" type="submit">Забронировать</button>
                                                <button class="btn btn-info btn-lg m-1 send-save" type="submit">Забронировать и запомнить</button>
                                                <button class="btn btn-info btn-lg m-1" type="reset">Сбросить</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- End find page -->
    <div id="floatingBarsG">
        <div class="blockG" id="rotateG_01"></div>
        <div class="blockG" id="rotateG_02"></div>
        <div class="blockG" id="rotateG_03"></div>
        <div class="blockG" id="rotateG_04"></div>
        <div class="blockG" id="rotateG_05"></div>
        <div class="blockG" id="rotateG_06"></div>
        <div class="blockG" id="rotateG_07"></div>
        <div class="blockG" id="rotateG_08"></div>
    </div>
    <div id="loginpopup" class="modal fade show" role="dialog" style="padding-right: 17px; display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5>Номерок успешно добавлен</h5>
                </div>
                <div class="modal-body text-center">
                    <div class="row">
                        <div class="col-md-12">
                            Вы будете перенаправлены на страницу поиска номерков
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('ajaxForm')
    <script src="{{asset('js/ajax.js')}}"></script>
    <script src="{{asset('js/ajax-pagination.js')}}"></script>
@endsection
