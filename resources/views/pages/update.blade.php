@extends('layouts.app')
@section('title', 'Добавить номерок')
@section('content')

    <div class="container-flex">
        <div id="form" class="col-sm-9">
            <div class="sub-ttl">Служебная информация</div>
            <div class="form-group">
                <label class="font-weight-bold" for="TicketCreatedBy">Номерок забронировал</label>
                <div>{{Auth::user()->name}} {{\Carbon\Carbon::parse($ticket->ticketcreateddate)->diffForHumans()}} </div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold" for="TicketStatus">Статус</label>
                <div>{{($ticket->ticketstatus == 2) ? 'Забронировано' : 'Свободно'}}</div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold" for="TicketId">Код номерка</label>
                <div>{{$ticket->appointmentticketid}}</div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold" for="TIcketOrganization">Учреждение</label>
                <div>{{$ticket->d_organization->name}} </div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold" for="">Код номерка из внешней системы</label>
                <div>{{ isset($ticket->ticketnumberexternal) ? $ticket->ticketnumberexternal : 'нет информации' }}</div>
            </div>
            <div class="form-group">
                <label class="font-weight-bold" for="TicketModifiedBy">Номерок обновил</label>
                <div>{{$ticket->user->name}} {{\Carbon\Carbon::parse($ticket->ticketmodifieddate)->diffForHumans()}} </div>
            </div>


            <form id="ajaxAddForm">
                {{csrf_field()}}
                <div class="sub-ttl">Реквизиты номерка</div>
                <div class="form-group">
                    <label for="patlastname">Фамилия пациента</label>
                    <input placeholder="" required class="form-control" type="text" name="patlastname" value="{{$ticket->patlastname}}">
                </div>
                <div class="form-group">
                    <label for="patfirstname">Имя пациента</label>
                    <input placeholder="" required class="form-control" type="text" name="patfirstname" value="{{$ticket->patfirstname}}">
                </div>
                <div class="form-group">
                    <label for="patpatronymicname">Отчество пациента</label>
                    <input placeholder="" required class="form-control" type="text" name="patpatronymicname" value="{{$ticket->patpatronymicname}}">
                </div>
                <div class="form-group">
                    <label for="patbirthdate">Дата рождения пациента</label>
                    <input placeholder="" required class="form-control" type="date" name="patbirthdate" value="{{$ticket->patbirthdate}}">
                </div>
                <label for="patpolicynum">Номер страхового полиса пациента</label>
                <div class="form-group">
                    <input placeholder="" required class="form-control" type="text" name="patpolicynum" value="{{$ticket->patpolicynum}}">
                </div>
                <div class="form-group">
                    <label for="patpolicynum">Дата окончания страхового полиса</label>
                    <input placeholder="" required class="form-control" type="date" name="patpolicydate" value="{{$ticket->patpolicydate}}">
                </div>
                <div class="form-group">
                    <label for="patphone">Номер телефона пациента</label>
                    <input placeholder="" required class="form-control" type="text" name="patphone" value="{{$ticket->patphone}}">
                </div>
                <div class="form-group">
                    <label for="patemail">E-mail пациента</label>
                    <input placeholder="" required class="form-control" type="text" name="patemail" value="{{$ticket->patemail}}">
                </div>
                <div class="form-group">
                    <label for="patvisitpurpose">Цель визита</label>
                    <input placeholder="" required class="form-control" type="text" name="patvisitpurpose" value="{{$ticket->patvisitpurpose}}">
                </div>
                <div class="form-group">
                    <label for="patcomment">Комментарии</label>
                    <textarea placeholder="{{ isset($ticket->patcomment) ? $ticket->patcomment : 'Нет комментариев'}}" class="form-control" rows="10" cols="45" name="extra">{{ isset($ticket->patcomment)}}</textarea>
                </div>


                <div class="form-group">
                    <p>Для записи на прием к врачу система должна передать через Интернет персональные данные. Если Вы с этим согласны, то поставьте отметку.</p>
                    <input class="checkbox" type="checkbox">
                </div>
                <input type="hidden" value="{{$ticket->appointmentticketid}}" name="appointmentticketid">
                <button disabled class="btn btn-info btn-lg m-1 submit" type="submit">Отправить</button>
            </form>
        </div>
@endsection

@section('ajaxUpdate')
    <script src="{{asset('js/ajax_update.js')}}"></script>
@endsection