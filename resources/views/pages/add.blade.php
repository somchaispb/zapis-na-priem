@extends('layouts.app')
@section('title', 'Добавить номерок')
@section('content')
    <ul class="breadcrumbs">
        @foreach ($breadcrumbs as $key => $url)
            <li class="{{ $loop->last ? 'is-active' : '' }}">
                @if (! $loop->last)
                    <a href="{{ url($url) }}">
                        {{ ucfirst($key) }}
                        ->
                        @else
                            @yield ('title')
                        @endif
                    </a>
            </li>
        @endforeach
    </ul>
    <div class="container-flex">
        <div id="form" class="col-sm-9">
            <form id="ajaxAddForm" action="{{action('PagesController@addForm')}}" method="post">
                {{csrf_field()}}
                <div class="sub-ttl">Специалист</div>
                <div id="form-errors"></div>
                <div class="form-group">
                    <label for="surname">Фамилия</label>
                    <input required class="form-control" type="text" name="SpecLastName" value="{{ old('surname') }}">
                </div>
                <div class="form-group">
                    <label for="name">Имя</label>
                    <input required class="form-control" type="text" name="SpecFirstName" value="{{ old('name') }}">
                </div>
                <div class="form-group">
                    <label for="thirdname">Отчество</label>
                    <input class="form-control" type="text" name="SpecPatronymicName" value="{{ old('thirdname') }}">
                </div>
                <div class="form-group">
                    <label for="SpecSpeciality">Специальность</label>
                    <select class="mdl-selectfield__select" name="SpecSpeciality">
                    @foreach($specialities as $speciality)
                        <option value="{{$speciality->specspeciality}}">{{$speciality->name}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="TicketService">Услуга</label>
                    <input class="form-control" type="text" name="TicketService" value="{{ old('TicketService') }}">
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="TicketDate">Дата с</label>
                        <input placeholder="дд.мм.гггг" required class="form-control" type="text" name="TicketDate" value="{{ old('TicketDate') }}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="TicketDate2">по</label>
                        <input placeholder="дд.мм.гггг" required class="form-control" type="text" name="TicketDate2" value="{{ old('TicketDate2') }}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="TicketTimeFrom">Время с</label>
                        <select class="mdl-selectfield__select" name="TicketTimeFrom">
                            @php
                                foreach($range as $time){
                                    echo '<option value="'.date("H:i",$time).'">'.date("H:i",$time).'</option>';
                                }
                            @endphp
                        </select>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="TicketTimeTo">по</label>
                        <select class="mdl-selectfield__select" name="TicketTimeTo">
                            @php
                                foreach($range as $time){
                                if (date("H:i",$time) == '17:00') {
                                    echo '<option selected="selected" value="'.date("H:i",$time).'">'.date("H:i",$time).'</option>';
                                    }else{
                                    echo '<option value="'.date("H:i",$time).'">'.date("H:i",$time).'</option>';
                                    }
                                }
                            @endphp
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="BreakFrom">Перерыв с</label>
                        <select class="mdl-selectfield__select" name="BreakFrom">
                            @php
                                foreach($range as $time){
                                if (date("H:i",$time) == '13:00') {
                                    echo '<option selected="selected" value="'.date("H:i",$time).'">'.date("H:i",$time).'</option>';
                                    }else{
                                    echo '<option value="'.date("H:i",$time).'">'.date("H:i",$time).'</option>';
                                    }
                                }
                            @endphp
                        </select>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="BreakTo">по</label>
                        <select class="mdl-selectfield__select" name="BreakTo">
                            @php
                                foreach($range as $time){
                                if (date("H:i", $time) == '14:00') {
                                    echo '<option selected="selected" value="'.date("H:i",$time).'">'.date("H:i",$time).'</option>';
                                    }else{
                                    echo '<option value="'.date("H:i",$time).'">'.date("H:i",$time).'</option>';
                                    }
                                }
                            @endphp
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="duration">Продолжительность</label>
                    <select class="mdl-selectfield__select" name="duration">
                        <option value="30">30 минут</option>
                        <option value="60">60 минут</option>
                    </select>
                </div>
                <div class="sub-ttl">Условия оплаты</div>
                <div class="form-group">
                    <label for="finance">Источники финансирования</label>
                    <input placeholder=""  class="form-control" type="text" name="finance" value="{{ old('finance') }}">
                </div>
                <div class="form-group">
                    <label for="price">Стоимость</label>
                    <input placeholder=""  class="form-control" type="text" name="price" value="{{ old('price') }}">
                </div>
                <div class="sub-ttl">Информация для пациента</div>
                <div class="form-group">
                    <label for="AddressNear">Рядом с</label>
                    <input placeholder="" class="form-control" type="text" name="AddressNear" value="{{ old('AddressNear') }}">
                </div>
                <div class="form-group">
                    <label for="TimeForMoving">Идти/ехать</label>
                    <input placeholder="" class="form-control" type="text" name="TimeForMoving" value="{{ old('TimeForMoving') }}">
                </div>
                <div class="form-group">
                    <label for="Recommendations">Рекомендации</label>
                    <input placeholder="" class="form-control" type="text" name="Recommendations" value="{{ old('Recommendations') }}">
                </div>
                <div class="sub-ttl">Реквизиты номерка</div>
                <div class="form-group">
                    <input placeholder="" required class="form-control" type="hidden" name="TicketPublicationDate" value="">
                    <label for="TicketStatus">Статус</label>
                    <div>Свободен</div>
                </div>
                <div class="form-group">
                    <label for="TicketId">Код номерка</label>
                    <div>Н1807-720-360-12</div>
                </div>
                <div class="form-group">
                    <label for="TIcketOrganization">Учреждение</label>
                    <select class="mdl-selectfield__select" name="TicketOrganization">
                        @foreach($appointments as $appointment)
                            <option value="{{$appointment->ticketorganization}}">{{$appointment->orgname}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Код номерка из внешней системы</label>
                    <div>НОМ 121/23</div>
                </div>
                <div class="form-group">
                    <label for="TicketCreatedBy">Создан</label>
                    <div>Уваров Петр Иванович</div>
                    <div>{{date('d:m:Y', mt_rand(1262055681,1362055681))}}</div>
                </div>
                <div class="form-group">
                    <label for="TicketModifiedBy">Последнее изменение</label>
                    <div>Мартынова Мария Васильевна</div>
                    <div>{{date('d:m:Y', mt_rand(1262055681,1362055681))}}</div>
                </div>
                <div class="form-group">
                    <p>Для записи на прием к врачу система должна передать через Интернет персональные данные. Если Вы с этим согласны, то поставьте отметку.</p>
                    <input type="checkbox">
                </div>
                <button disabled class="btn btn-info btn-lg m-1 submit" type="submit">Отправить</button>
            </form>
        </div>
        @endsection

        @section('ajaxForm')
            <script src="{{asset('js/ajax_add.js')}}"></script>
@endsection