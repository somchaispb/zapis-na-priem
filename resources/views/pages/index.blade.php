@extends('layouts.app')
@section('title', 'Список номерков')
@section('content')
    {{--<ul class="breadcrumbs">--}}
        {{--@foreach ($breadcrumbs as $key => $url)--}}
            {{--<li class="{{ $loop->last ? 'is-active' : '' }}">--}}
                {{--@if (! $loop->last)--}}
                    {{--<a href="{{ url($url) }}">--}}
                        {{--{{ ucfirst($key) }}--}}
                        {{--->--}}
                        {{--@else--}}
                            {{--@yield ('title')--}}
                        {{--@endif--}}
                    {{--</a>--}}
            {{--</li>--}}
        {{--@endforeach--}}
    {{--</ul>--}}
    <!-- Start Page Title Section -->
    <div class="page-ttl">
        <div class="layer-stretch">
            <div class="page-ttl-container">
                <h1>Главная</h1>
                <p><a href="/">Мои номерки</a></p>
                @if (session('status'))
                    <div class="alert alert-success alert-flash">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
        </div>
    </div><!-- End Page Title Section -->
    <!-- Start My Appointment Section -->
    <div id="myappointment-page" class="layer-stretch">
        <div class="layer-wrapper layer-bottom-0">
            <div class="row myappointment-container">

                @foreach($tickets as $ticket)
                    <div class="col-md-4">
                        <div class="theme-material-card">
                            <div class="myappointment-text">
                                <i class="fa fa-id-card"></i>
                                <span class="paragraph-medium paragraph-black">ID - {{$ticket->appointmentticketid}}</span>
                            </div>
                            <div class="myappointment-text">
                                <i class="fa fa-user-md"></i>
                                <span class="paragraph-medium paragraph-black">{{$ticket->specfullname}}</span>
                            </div>
                            <div class="myappointment-text">
                                <i class="fa fa-user-o"></i>
                                <span class="paragraph-medium paragraph-black">{{$ticket->patfullname}}</span>
                            </div>
                            <div class="myappointment-text">
                                <i class="fa fa-calendar-plus-o"></i>
                                <span class="paragraph-medium paragraph-black">{{$ticket->ticketdate}}</span>
                            </div>
                            <div class="myappointment-text">
                                <i class="fa fa-clock-o"></i>
                                <span class="paragraph-medium paragraph-black">{{$ticket->tickettime}}</span>
                            </div>
                            <div class="myappointment-text">
                                <i class="fa fa-flag-o"></i>
                                <span class="paragraph-medium paragraph-black">
                                    @if($ticket->ticketstatus == 2)
                                        Забронировано
                                    @else
                                        В ожидании
                                    @endif
                                </span>
                            </div>
                            <div class="myappointment-view">
                                <a href="/show/{{$ticket->appointmentticketid}}" class="mdl-button mdl-js-button mdl-button--colored mdl-js-ripple-effect mdl-button--raised button button-primary button-sm pull-right">Подробнее</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div><!-- End My Appointment Section -->
    <div id="emergency">
        <div class="layer-stretch">
            <div class="layer-wrapper">
                <div class="layer-ttl">
                    <h3>Служба поддержки</h3>
                </div>
                <div class="layer-container">
                    <div class="paragraph-medium paragraph-black">
                        В случае вопросов, звоните
                    </div>
                    <div class="emergency-number">Тел : 0000000000</div>
                </div>
            </div>
        </div>
    </div>
@endsection