{{--<!-- Start Footer Section -->--}}
{{--<footer id="footer">--}}
    {{--<!-- Start Copyright Section -->--}}
    {{--<div id="copyright">--}}
        {{--<div class="layer-stretch">--}}
            {{--<div class="paragraph-medium paragraph-white">@php echo date('Y'); @endphp © VitaSoft ALL RIGHTS RESERVED.</div>--}}
        {{--</div>--}}
    {{--</div><!-- End of Copyright Section -->--}}
{{--</footer><!-- End of Footer Section -->--}}
{{--<!-- Start Make an Appointment Modal -->--}}
{{--<div id="appointment" class="modal fade" role="dialog">--}}
    {{--<div class="modal-dialog modal-lg">--}}
        {{--<div class="modal-content">--}}
            {{--<div class="modal-header text-center">--}}
                {{--<h5 class="modal-title">Make An Appointment</h5>--}}
                {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
            {{--</div>--}}
            {{--<div class="modal-body">--}}
                {{--<div class="appointment-error"></div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-input-icon">--}}
                            {{--<i class="fa fa-user-o"></i>--}}
                            {{--<input class="mdl-textfield__input" type="text" pattern="[A-Z,a-z, ]*" id="appointment-name">--}}
                            {{--<label class="mdl-textfield__label" for="appointment-name">Name</label>--}}
                            {{--<span class="mdl-textfield__error">Please Enter Valid Name!</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-input-icon">--}}
                            {{--<i class="fa fa-envelope-o"></i>--}}
                            {{--<input class="mdl-textfield__input" type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" id="appointment-email">--}}
                            {{--<label class="mdl-textfield__label" for="appointment-email">Email</label>--}}
                            {{--<span class="mdl-textfield__error">Please Enter Valid Email!</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-input-icon">--}}
                            {{--<i class="fa fa-phone"></i>--}}
                            {{--<input class="mdl-textfield__input" type="text" pattern="[0-9]*" id="appointment-mobile">--}}
                            {{--<label class="mdl-textfield__label" for="appointment-mobile">Mobile Number</label>--}}
                            {{--<span class="mdl-textfield__error">Please Enter Valid Mobile Number!</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="mdl-selectfield mdl-js-selectfield mdl-selectfield--floating-label form-input-icon">--}}
                            {{--<i class="fa fa-hospital-o"></i>--}}
                            {{--<select class="mdl-selectfield__select" id="appointment-department">--}}
                                {{--<option value="">&nbsp;</option>--}}
                                {{--<option value="1">Gynaecology</option>--}}
                                {{--<option value="2">Orthology</option>--}}
                                {{--<option value="3">Dermatologist</option>--}}
                                {{--<option value="4">Anaesthesia</option>--}}
                                {{--<option value="5">Ayurvedic</option>--}}
                            {{--</select>--}}
                            {{--<label class="mdl-selectfield__label" for="appointment-department">Choose Department</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="mdl-selectfield mdl-js-selectfield mdl-selectfield--floating-label form-input-icon">--}}
                            {{--<i class="fa fa-user-md"></i>--}}
                            {{--<select class="mdl-selectfield__select" id="appointment-doctor">--}}
                                {{--<option value="">&nbsp;</option>--}}
                                {{--<option value="1">Dr. Daniel Barnes</option>--}}
                                {{--<option value="2">Dr. Steve Soeren</option>--}}
                                {{--<option value="3">Dr. Barbara Baker</option>--}}
                                {{--<option value="4">Dr. Melissa Bates</option>--}}
                                {{--<option value="5">Dr. Linda Adams</option>--}}
                            {{--</select>--}}
                            {{--<label class="mdl-selectfield__label" for="appointment-doctor">Choose Doctor</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-input-icon">--}}
                            {{--<i class="fa fa-calendar-o"></i>--}}
                            {{--<input class="mdl-textfield__input" type="text" id="appointment-date" onfocus="(this.type='date')" onblur="(this.type='text')">--}}
                            {{--<label class="mdl-textfield__label" for="appointment-date">Date</label>--}}
                            {{--<span class="mdl-textfield__error">Please Enter Valid Date Number!</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="text-center pt-4">--}}
                    {{--<button class="mdl-button mdl-js-button mdl-button--colored mdl-js-ripple-effect mdl-button--raised button button-primary button-lg make-appointment">Submit</button>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div><!-- End Make an Appointment Modal -->--}}
{{--<!-- Start Login Modal -->--}}
{{--<div id="loginpopup" class="modal fade" role="dialog">--}}
    {{--<div class="modal-dialog">--}}
        {{--<div class="modal-content">--}}
            {{--<div class="modal-header text-center">--}}
                {{--<h5 class="modal-title">Login Now</h5>--}}
                {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
            {{--</div>--}}
            {{--<div class="modal-body text-center">--}}
                {{--<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-input-icon">--}}
                    {{--<i class="fa fa-envelope-o"></i>--}}
                    {{--<input class="mdl-textfield__input" type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" id="loginpopup-email">--}}
                    {{--<label class="mdl-textfield__label" for="loginpopup-email">Email <em> *</em></label>--}}
                    {{--<span class="mdl-textfield__error">Please Enter Valid Email!</span>--}}
                {{--</div>--}}
                {{--<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-input-icon">--}}
                    {{--<i class="fa fa-key"></i>--}}
                    {{--<input class="mdl-textfield__input" type="password" id="loginpopup-password">--}}
                    {{--<label class="mdl-textfield__label" for="loginpopup-password">Password <em> *</em></label>--}}
                    {{--<span class="mdl-textfield__error">Please Enter Valid Password!</span>--}}
                    {{--<div class="forgot-pass">Forgot Password?</div>--}}
                {{--</div>--}}
                {{--<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-input-icon form-bot-check">--}}
                    {{--<i class="fa fa-question"></i>--}}
                    {{--<input class="mdl-textfield__input" type="number" id="loginpopup-bot">--}}
                    {{--<label class="mdl-textfield__label" for="loginpopup-bot">What is 7 plus 1 = <em>* </em></label>--}}
                    {{--<span class="mdl-textfield__error">Please Enter Correct Value!</span>--}}
                {{--</div>--}}
                {{--<div class="form-submit">--}}
                    {{--<button class="mdl-button mdl-js-button mdl-js-ripple-effect button button-primary">Login Now</button>--}}
                {{--</div>--}}
                {{--<div class="or-using">Or Using</div>--}}
                {{--<div class="social-login">--}}
                    {{--<a href="#" class="social-facebook"><i class="fa fa-facebook"></i>Facebook</a>--}}
                    {{--<a href="#" class="social-google"><i class="fa fa-google"></i>Google</a>--}}
                {{--</div>--}}
                {{--<div class="login-link">--}}
                    {{--<span class="paragraph-small">Don't have an account?</span>--}}
                    {{--<a href="#" class="">Register as New User</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div><!-- End Login Modal -->--}}
{{--<!-- Start Register Modal -->--}}
{{--<div id="registerpopup" class="modal fade" role="dialog">--}}
    {{--<div class="modal-dialog">--}}
        {{--<div class="modal-content">--}}
            {{--<div class="modal-header text-center">--}}
                {{--<h5 class="modal-title">Register as New User</h5>--}}
                {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
            {{--</div>--}}
            {{--<div class="modal-body text-center">--}}
                {{--<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-input-icon">--}}
                    {{--<i class="fa fa-user-o"></i>--}}
                    {{--<input class="mdl-textfield__input" type="text" pattern="[A-Z,a-z, ]*" id="registerpopup-name">--}}
                    {{--<label class="mdl-textfield__label" for="registerpopup-name">Name <em> *</em></label>--}}
                    {{--<span class="mdl-textfield__error">Please Enter Valid Name!</span>--}}
                {{--</div>--}}
                {{--<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-input-icon">--}}
                    {{--<i class="fa fa-envelope-o"></i>--}}
                    {{--<input class="mdl-textfield__input" type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" id="registerpopup-email">--}}
                    {{--<label class="mdl-textfield__label" for="registerpopup-email">Email <em> *</em></label>--}}
                    {{--<span class="mdl-textfield__error">Please Enter Valid Email!</span>--}}
                {{--</div>--}}
                {{--<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-input-icon">--}}
                    {{--<i class="fa fa-key"></i>--}}
                    {{--<input class="mdl-textfield__input" type="password" id="registerpopup-password">--}}
                    {{--<label class="mdl-textfield__label" for="registerpopup-password">Password <em> *</em></label>--}}
                    {{--<span class="mdl-textfield__error">Please Enter Valid Password(Min 6 Character)!</span>--}}
                {{--</div>--}}
                {{--<div class="login-condition">By clicking Creat Account you agree to our <a href="#">terms &#38; condition</a></div>--}}
                {{--<div class="form-submit">--}}
                    {{--<button class="mdl-button mdl-js-button mdl-js-ripple-effect button button-primary">Create Account</button>--}}
                {{--</div>--}}
                {{--<div class="or-using">Or Using</div>--}}
                {{--<div class="social-login">--}}
                    {{--<a href="#" class="social-facebook"><i class="fa fa-facebook"></i>Facebook</a>--}}
                    {{--<a href="#" class="social-google"><i class="fa fa-google"></i>Google</a>--}}
                {{--</div>--}}
                {{--<div class="login-link">--}}
                    {{--<span class="paragraph-small">Already have an account?</span>--}}
                    {{--<a href="#" class="">Login Now</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div><!-- End Register Modal -->--}}
<!-- Fixed Appointment Button at Bottom -->
<div id="appointment-button" class="animated fadeInUp">
    <button id="appointment-now" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-button--raised"><i class="fa fa-arrow-up"></i></button>
</div><!-- End Fixed Appointment Button at Bottom -->

<!-- **********Included Scripts*********** -->
<!-- Moment Plugin JavaScript  ( For Event plugin )-->
<script src="{{asset('js/moment.min.js')}}"></script>
<!-- Jquery Library 2.1 JavaScript-->
<script src="{{asset('js/jquery-2.1.4.min.js')}}"></script>
<!-- Jquery Cookie-->
<script src="{{asset('js/jquery.cookie.js')}}"></script>
<!-- Popper JavaScript-->
<script src="{{asset('js/popper.min.js')}}"></script>
<!-- Bootstrap Core JavaScript-->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- Material Design Lite JavaScript-->
<script src="{{asset('js/material.min.js')}}"></script>
<!-- Material Select Field Script -->
<script src="{{asset('js/mdl-selectfield.min.js')}}"></script>
<!-- Flexslider Plugin JavaScript-->
<script src="{{asset('js/jquery.flexslider.min.js')}}"></script>
<!-- Owl Carousel Plugin JavaScript-->
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<!-- Scrolltofixed Plugin JavaScript-->
<script src="{{asset('js/jquery-scrolltofixed.min.js')}}"></script>
<!-- Magnific Popup Plugin JavaScript-->
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<!-- FullCalendar Plugin JavaScript  ( Event plugin )-->
<script src="{{asset('js/fullcalendar.min.js')}}"></script>
<!-- WayPoint Plugin JavaScript-->
<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
<!-- CounterUp Plugin JavaScript-->
<script src="{{asset('js/jquery.counterup.js')}}"></script>
<!-- SmoothScroll Plugin JavaScript-->
<script src="{{asset('js/smoothscroll.min.js')}}"></script>
<!-- MaskedInput Plugin JavaScript-->
<script src="{{asset('js/jquery.maskedinput.js')}}"></script>
<!--Custom JavaScript for Klinik Template-->
<script src="{{asset('js/custom.js')}}"></script>
