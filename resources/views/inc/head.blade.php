<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Site Title -->
<title>@yield('title')</title>
<!-- Meta Description Tag -->
<meta name="Description" content="prototype zapis na priem">
<!-- Favicon Icon -->
<link rel="icon" type="image/x-icon" href="{{asset('images/favicon.png')}}" />
<!-- Font Awesoeme Stylesheet CSS -->
<link rel="stylesheet" href="{{asset('font-awesome/css/font-awesome.min.css') }}" />
<!-- Google web Font -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
<!-- Material Design Lite Stylesheet CSS -->
<link rel="stylesheet" href="{{asset('css/material.min.css') }}" />
<!-- Material Design Select Field Stylesheet CSS -->
<link rel="stylesheet" href="{{asset('css/mdl-selectfield.min.css') }}">
<!-- Owl Carousel Stylesheet CSS -->
<link rel="stylesheet" href="{{asset('css/owl.carousel.min.css') }}" />
<!-- Animate Stylesheet CSS -->
<link rel="stylesheet" href="{{asset('css/animate.min.css') }}" />
<!-- Magnific Popup Stylesheet CSS -->
<link rel="stylesheet" href="{{asset('css/magnific-popup.css') }}" />
<!-- Full Calendar Stylesheet CSS   ( For Event plugin )-->
<link rel="stylesheet" href="{{asset('css/fullcalendar.min.css')}}">
<!-- Flex Slider Stylesheet CSS -->
<link rel="stylesheet" href="{{asset('css/flexslider.css') }}" />
<!-- Custom Main Stylesheet CSS -->
<link rel="stylesheet" href="{{asset('css/style.css') }}">