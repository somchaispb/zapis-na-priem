<!-- Start Header -->
<header id="header">
    <!-- Start Header Top Section -->
    <div id="hdr-top-wrapper">
        <div class="layer-stretch hdr-top">
            <div class="hdr-top-block">
                @if (Auth::guest())
                    <a href="{{ route('login') }}">Войти</a>
                    <div class="hdr-top-line hidden-xs"></div>
                    <a href="{{ route('register') }}">Зарегистрироваться</a>
                    <!-- Authentication Links -->
                @else
                    <span>Привет, {{ Auth::user()->name }}</span>
                    <div class="hdr-top-line hidden-xs"></div>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        Выйти
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @endif
            </div>
        </div>
    </div><!-- End Header Top Section -->
    <!-- Start Main Header Section -->
    <div id="hdr-wrapper">
        <div class="layer-stretch hdr">
            <div class="tbl">
                <div class="tbl-row">
                    <!-- Start Header Logo Section -->
                    <div class="tbl-cell hdr-logo">
                        <a href="/"><img src="{{asset('images/logo.png')}}" alt=""></a>
                    </div><!-- End Header Logo Section -->
                    <div class="tbl-cell hdr-menu">
                        <!-- Start Menu Section -->
                        <ul class="menu">
                            <li>
                                <a id="menu-home" class="mdl-button mdl-js-button mdl-js-ripple-effect">Номерки <i class="fa fa-chevron-down"></i></a>
                                <ul class="menu-dropdown">
                                    <li><a href="{{action('PagesController@add')}}">Добавить номерок</a></li>
                                    <li><a href="{{action('PagesController@find')}}">Найти номерок</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{action('TicketController@index')}}">Операции над номерками</a>
                            </li>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Main Header Section -->
</header><!-- End Header -->