@if(count($errors) > 0)
    @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                {{$error}}
            </div>
    @endforeach
@endif

@if(session('success'))
    <div class="alert alert-success alert-flash">
        {{session('success')}}
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger alert-flash">
        {{session('error')}}
    </div>
@endif