@extends('layouts.app')
@section('content')
<!-- Start Not Found Section -->
<div class="notfound-wrapper">
    <h1 class="notfound-ttl">404</h1>
    <p class="notfound-tag-1">Страница не найдена!!!</p>
    <span class="notfound-tag-2">Такой страницы не существует...</span>
    <div class="notfound-link">
        <a href="/" class="button-icon"><span>Вернуться на главную</span><i class="fa fa-home"></i></a>
        <a href="mailto:admin@admin.ad" class="button-icon"><span>Сообщить о проблеме</span><i class="fa fa-bug"></i></a>
    </div>
</div><!-- End Not Found Section -->
@endsection