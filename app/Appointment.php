<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Appointment extends Model
{
    protected $table = 'v_orgtab';

    /**
     * @param null $dateFrom
     * @param null $dateTo
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getFunction($dateFrom = null, $dateTo = null) {
        $dateFrom = $dateFrom ? $dateFrom : Carbon::now()->format('Y-m-d');
        $dateTo = $dateTo ? $dateTo : Carbon::now()->addWeek(1)->format('Y-m-d');

        return self::hydrate(DB::select(
            'select * from fn_orgtab(\''.$dateFrom.'\'::date, \''.$dateTo.'\'::date)'
        ));
    }
}
