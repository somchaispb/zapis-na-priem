<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 12/11/18
 * Time: 14:01
 */

namespace App\Composers;
use Illuminate\View\View;
use Illuminate\Http\Request;

class BreadcrumbComposer
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Initialize a new composer instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('breadcrumbs', $this->parseSegments());
    }

    /**
     * Parse the request route segments.
     *
     * @return \Illuminate\Support\Collection
     */
    protected function parseSegments()
    {
        return collect($this->request->segments())->mapWithKeys(function ($segment, $key) {
            switch ($segment) {
                case 'add':
                    $segment = 'Добавить номерок';
                    break;
                case 'show':
                    $segment = 'Просмотр данных номерка';
                    break;
                case 'find':
                    $segment = 'Поиск номерков';
                    break;
                default:
                    true;
            }
//            if ($this->request->getBaseUrl() == "") $segment = 'Главная';
//            dd($segment);
            return [
                $segment => implode('/', array_slice($this->request->segments(), 0, $key + 1))
            ];
        });
    }
}