<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Composers\BreadcrumbComposer;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        setlocale(LC_TIME, 'ru_RU.UTF-8');
        \Carbon\Carbon::setLocale(config('app.locale'));
        View::composer('pages.*', BreadcrumbComposer::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
