<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class D_organization extends Model
{
    protected $table = 'd_organization';
    protected $primaryKey = 'organizationid';

    public function entireTable() {
        return $this->hasOne('App\EntireTable', 'fk_org', 'organizationid');
    }
}