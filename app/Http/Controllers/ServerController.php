<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use SSH;

class ServerController extends Controller
{
    public function deploy() {

        SSH::into('production')->run(array(
            'cd /usr/share/nginx/html/zapis/html',
            'git pull origin master'
        ), function($line){
            echo $line.PHP_EOL; // outputs server feedback
        });
    }

    public function apptest(Request $request) {
        $data = json_decode($request->getContent(), true);
        $user = null;
        $users = [
            'email' => 'user',
            'password' => 'test'
        ];
        if($users['email'] == $data['email']) $user['email'] = $data['email'];
        if ($users['password'] == $data['password']) $user['password'] = $data['password'];

        if (!empty($user['email']) && !empty($user['password'])) {
            return Response::create($user, 200);
        }
        else
            return Response::create('Status: failed', 401);
    }
}
