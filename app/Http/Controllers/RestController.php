<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\EntireTable;
use Tymon\JWTAuth\Facades\JWTAuth;


class RestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$request->method('GET')) {
            $code = 405;
        }else{
            $user = JWTAuth::parseToken()->authenticate();
            $limit = $request->input('limit') ? (int)$request->input('limit') : 5;
            $order = $request->input('orderby') ? $request->input('orderby') : 'desc';
            $tickets = (new EntireTable())->where('ticketbookedby', $user->id)->limit($limit)->orderBy('appointmentticketid', $order)->get();
            $code = 200;
        }
        return response()->json(array('status' => 'show all', 'response' => compact('tickets')), $code);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function create(Request $request)
    {
        if ($request->method('POST')) {
            $newAppointment = new EntireTable();
            $this->validate($request, $newAppointment->rulesAdd);
            $data = $request->input();
            $breakFrom = $data['BreakFrom'];
            $breakTo = $data['BreakTo'];
            $ticketDateFrom = new \DateTime( $data['TicketDate']. $data['TicketTimeFrom']);
            $ticketDateTo = new \DateTime($data['TicketDate2']. $data['TicketTimeTo']);
            $TicketTimeFrom = $data['TicketTimeFrom'];
            $TicketTimeTo = $data['TicketTimeTo'];
            $step = "PT".$data['duration']."M";
            $interval = new \DateInterval($step);
            $dateRange = new \DatePeriod($ticketDateFrom, $interval, $ticketDateTo);
            $user = JWTAuth::parseToken()->authenticate();
            $query = [];
            $count = 0;
            foreach ($dateRange as $k => $item){
                // Выпиливаем время до 10.00 и после 17.00 (оставляем тока рабочее)
                if ($item->format('H:i') < $TicketTimeFrom || $item->format('H:i') >= $TicketTimeTo) continue;
                // Выпиливаем перерыв на обед
                elseif ($item->format('H:i') >= $breakFrom && $item->format('H:i') < $breakTo) continue;
                else $query[$k] = [
                    'speclastname' => $data['SpecLastName'],
                    'specfirstname' => $data['SpecFirstName'],
                    'specpatronymicname' => $data['SpecPatronymicName'],
                    'specfullname' => $data['SpecLastName']. ' ' .$data['SpecFirstName']. ' ' .$data['SpecPatronymicName'],
                    'ticketpublicationdate' => Carbon::now(),
                    'ticketorganization' => $data['TicketOrganization'],
                    'ticketcreatedby' => $user->id,
                    'ticketcreateddate' => Carbon::now(),
                    'ticketmodifiedby' => 1,
                    'ticketmodifieddate' => Carbon::now(),
                    'specspeciality' => $data['SpecSpeciality'],
                    'ticketservice' => $data['TicketService'],
                    'addressnear' => $data['AddressNear'],
                    'timeformoving' => $data['TimeForMoving'],
                    'recommendations' => $data['Recommendations'],
                    'ticketdate' => $item->format('Y-m-d'),
                    'tickettime' => $item->format('H:i'),
                    'ticketstatus' => 1
                ];
                $count++;
            }
            try {
                $newAppointment->insert($query);
            } catch (\Exception $exception) {
                return 'Error occured while saving to database '. $exception->getMessage();
            } finally {
                $code = 201;
                $status = 'created';
                return response()->json(array('status' => $status, 'rows' => $count), $code);
            }
        } else {
            $code = 405;
            return response()->json(array('status' => 'Method not allowed'), $code);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (is_numeric($id)) {
            $user = JWTAuth::parseToken()->authenticate();
            $ticket = (new EntireTable())->where('appointmentticketid', $id)->where('ticketcreatedby', $user->id)->get();
            $status = 'show';
        } else {
            $status = 'your id is not numeric';
        }
        return response()->json(array('status' => $status, 'ticket' => compact('ticket')));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id) //Какие поля давать для обновления?
    {
        $ticket = [];
        if (is_numeric($id)) {
            $ticket = EntireTable::find($id);
            $params = $request->query();
            if ($ticket) {
                $ticket->ticketservice = $request->input('ticketservice') ? $request->input('ticketservice') : $ticket->ticketservice;
                $ticket->save();
                $status = "Ticket $id updated successfully";
                $code = 202;
            }else {
                $status = 'Ticket not found';
                $code = 204;
            }
        }else {
            $status = 'Ticket ID should be numeric';
            $code = 406;
        }
        return response()->json(array('status' => $status, 'ticket' => $ticket), $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        echo $request->getContent();
        return response()->json(array('status' => 'deleted'));
    }


    public function destroy(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $tickets = $request->input();
        $validIDs = [];
        if (empty($tickets['id'])) {
            $status = 'Please fill ID';
            $code = 204;
        } else {
            if(ctype_digit(implode('',$tickets['id']))) {
                // Проверка - принадлежит ли юзеру и существующие ID
                $rows = EntireTable::whereIn('appointmentticketid', $tickets['id'])->where('ticketcreatedby', $user->id)->get();
                if ($rows->count() <= 0) return response()->json(array('status' => 'Not found. One ore more ID\'s are incorrect'), 404);
                else {
                    foreach ($rows as $row) {
                        $validIDs[] = $row->appointmentticketid;
                    }
                    EntireTable::whereIn('appointmentticketid', $validIDs)->delete();
                    $status = "Tickets deleted successfully, id: ";
                    $status .= implode(',', $validIDs);
                    $code = 202;
                }
            }else{
                $status = 'Ticket ID should be numeric';
                $code = 406;
            }
        }
        return response()->json(array('status' => $status, 'id' => $validIDs), $code);
    }
}
