<?php

namespace App\Http\Controllers;

use App\EntireTable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Appointment;
use App\Speciality;
use App\Employee;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    protected $timeNow;

    public function __construct()
    {
        $this->middleware('auth');
        $this->timeNow = Carbon::now();
    }

    public function index(){
//        $functionData = DB::select('select fn1_test(?,?) as result', array(1,200));
//        $tickets = EntireTable::all()->where('ticketbookedby', Auth::user()->id);
        $tickets = (new EntireTable())->where('ticketbookedby', Auth::user()->id)->limit(6)->get();
        return view('pages.index', compact( 'tickets'));
    }

    public function find(Request $request){
        Redis::flushDB();
        Redis::set('dateFrom', Carbon::now()->format('Y-m-d'));
        Redis::set('dateTo', Carbon::now()->addWeek(1)->format('Y-m-d'));
//        $page = $request->has('page') ? $request->query('page') : 1;
        $appointments = (new Appointment())->getFunction();
        $employees = (new Employee())->getFunction();
        $specialities = (new Speciality())->getFunction();
        $dates = [$this->timeNow->format('d-m-Y'), $this->timeNow->addWeek(1)->format('d-m-Y')];
        if($request->ajax() && $request->method('get')) {
            return view('ajax.employee', compact('employees'));
        }

        return view('pages.find', compact('appointments', 'specialities', 'employees', 'dates'));
    }

    public function add(){
        $range = range(strtotime("10:00"),strtotime("17:00"),30*60);
        $appointments = Appointment::orderBy('orgname')->get();
        $specialities = Speciality::orderBy('name')
            ->get()
            ->unique('name');
        return view('pages.add', compact('range', 'appointments', 'specialities'));
    }

    public function addForm(Request $request){
        if($request->ajax()){
            $newAppointment = new EntireTable();
            $this->validate($request, $newAppointment->rulesAdd);
            $data = $request->input();
            // Магия разбиения номерков по датам и времени
            $breakFrom = $data['BreakFrom'];
            $breakTo = $data['BreakTo'];
            $ticketDateFrom = new \DateTime( $data['TicketDate']. $data['TicketTimeFrom']);
            $ticketDateTo = new \DateTime($data['TicketDate2']. $data['TicketTimeTo']);
            $TicketTimeFrom = $data['TicketTimeFrom'];
            $TicketTimeTo = $data['TicketTimeTo'];
            $step = "PT".$data['duration']."M";
            $interval = new \DateInterval($step);
            $dateRange = new \DatePeriod($ticketDateFrom, $interval, $ticketDateTo);
            $query = [];
            foreach ($dateRange as $k => $item){
                // Выпиливаем время до 10.00 и после 17.00 (оставляем тока рабочее)
                if ($item->format('H:i') < $TicketTimeFrom || $item->format('H:i') >= $TicketTimeTo) continue;
                // Выпиливаем перерыв на обед
                elseif ($item->format('H:i') >= $breakFrom && $item->format('H:i') < $breakTo) continue;
                else $query[$k] = [
                    'speclastname' => $data['SpecLastName'],
                    'specfirstname' => $data['SpecFirstName'],
                    'specpatronymicname' => $data['SpecPatronymicName'],
                    'specfullname' => $data['SpecLastName']. ' ' .$data['SpecFirstName']. ' ' .$data['SpecPatronymicName'],
                    'ticketpublicationdate' => Carbon::now(),
                    'ticketorganization' => $data['TicketOrganization'],
                    'ticketcreatedby' => 1,
                    'ticketcreateddate' => Carbon::now(),
                    'ticketmodifiedby' => 1,
                    'ticketmodifieddate' => Carbon::now(),
                    'specspeciality' => $data['SpecSpeciality'],
                    'ticketservice' => $data['TicketService'],
                    'addressnear' => $data['AddressNear'],
                    'timeformoving' => $data['TimeForMoving'],
                    'recommendations' => $data['Recommendations'],
                    'ticketdate' => $item->format('Y-m-d'),
                    'tickettime' => $item->format('H:i'),
                    'ticketstatus' => 1
                ];

            }
            try {
                $newAppointment->insert($query);
            } catch (\Exception $exception) {
                return 'Error occured while saving to database'. $exception->getMessage();
            } finally {
                $request->session()->flash('success', 'Номерок успешно добавлен');
                return response()->json(array('success' => 'true'));
//                return redirect('/find');
            }
        }
    }

    public function show($id){
        // найти в таблице сущность номерка
        // вывести сущноть во вьюху по ID
        $uid = Auth::user()->id;
        $ticket = EntireTable::find($id);
//        $ticket->ticketmodifiedby = User::find($ticket->ticketmodifiedby);
        if($ticket->ticketbookedby !== $uid) echo 'You dont have permission to watch that stuff';
        return view('pages.show', compact('ticket'));
    }


    public function updateticket (Request $request) {
        if ($request->ajax() && $request->method('put')) {
            $status = [];
            $url = explode('/', $request->server('HTTP_REFERER'));
            $data = $request->input();
            if (!in_array($data['appointmentticketid'], $url)) return response()->json(['status' => 'id does not match']);
            $newAppointment = EntireTable::find($data['appointmentticketid']);
            $this->validate($request, $newAppointment->rulesUpdate);
            if ($newAppointment->update($data)) $status['status'] = 'updated';
            else
                $status['status'] = 'error';
            return response()->json($status);
        }
    }



    public function serverTime(Request $request) {
        if($request->ajax() && $request->method('get')) {
            if($request->reset) {
                Redis::del('sessionFinishes');

            }elseif ($request->status) {
                dd($request->status);
            }else {
                return json_encode(['status' => 'not allowed here']);
            }

//            return Redis::get('sessionFinishes');
        }
    }

}
