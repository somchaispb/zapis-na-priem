<?php

namespace App\Http\Controllers;

use App\EntireTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = (new EntireTable())->where('ticketbookedby', Auth::user()->id)->orderBy('ticketdate', 'Desc')->get();
        return view('ticket.index', compact( 'tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $oldTicket = EntireTable::find($id);
        Session::put('oldTicket', $oldTicket);
        return redirect('/find');
    }

    /**
     * @param Request $request
     * @param $id
     * @return void
     */
    public function editForm(Request $request, $id)
    {
        if ($request->method('post')) {
            dd(Redis::get('appointmentticketid'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $ticket = EntireTable::find($id);
        return view('pages.update', compact('ticket'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(Request $request)
    {
        if($request->ajax() && $request->method('post')){
            if($request->input('action') == 'cancel') {
                EntireTable::whereIn('appointmentticketid', $request->id)->update([
                    'ticketstatus' => 1,
                    'ticketbookedby' => null
                    ]);
                return response()->json(['status' => 'canceled']);
            }
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Request $request, $id) {
        if ($request->ajax() && $request->method('delete')) {
            if($request->input('action') == 'delete') {
                $status = [];
                $appointment = EntireTable::find($id);
                if ($appointment->delete()) $status['status'] = 'deleted';
                else
                    $status['status'] = 'error';
                return response()->json($status);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        if($request->ajax() && $request->method('post')){
            if($request->input('action') == 'delete-bulk') {
                $status = [];
                if (EntireTable::whereIn('appointmentticketid', $request->id)->delete()) $status['status'] = 'deleted';
                else
                    $status['status'] = 'error';
                return response()->json($status);
            }
        }

    }
}
