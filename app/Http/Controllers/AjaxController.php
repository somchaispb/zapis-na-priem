<?php

namespace App\Http\Controllers;

use App\Appointment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Speciality;
use App\Employee;
use App\Date;
use App\EntireTable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;


class AjaxController extends Controller
{
    protected $timeNow;

    public function __construct()
    {
        $this->timeNow = Carbon::now();
    }

    public function ajaxForm(Request $request) {
        $duration = 10;
        if($request->ajax() && $request->method('post')) {
            $item = json_decode($request->getContent());
            // получаем $item->id из ajax
            $this->validate($request, [
                'id.*' => 'string',
                'name' => 'string|nullable'
            ]);
            $id = isset($item->id) ? $item->id : null;
            $name = isset($item->name) ? $item->name : null;
            // Обработчик первой вкладки - Клиника
            if($item->ParentTab == 'ticketorganization') {
                // сохраняем в редис
                if (!is_array($item->id)) Redis::hmset('user:'.Auth::user()->name.':where', $item->ParentTab, $id);
                // на основе данных делаем запрос к БД
                $specialities = (new Speciality)->getFunction(Redis::get('dateFrom'), Redis::get('dateTo'), $id);
                $employees = (new Employee())->getFunction(Redis::get('dateFrom'), Redis::get('dateTo'), $id)->sortBy('specfullname');
                $returnHTML = view('ajax.speciality')->with(compact('specialities'))->render();
                $returnHTMLEmployees = view('ajax.employee')->with(compact('employees'))->render();
//                $breadCrumbsHtml = view('ajax.breadcrumbs')->with(compact('specialities'))->render();
                $returnArray = array('tab' => 'specspeciality', 'html' => $returnHTML, 'employeesHtml' => $returnHTMLEmployees);
            }
            // Обработчик второй вкладки - Специальность
            elseif ($item->ParentTab == 'specspeciality') {
                if (isset($item->clear)) {
                    Redis::hdel('user:'.Auth::user()->name.':where', $item->tab);
                    $specialities = (new Speciality)->getFunction(Redis::get('dateFrom'), Redis::get('dateTo'));
                    $returnHTML = view('ajax.speciality')->with(compact('specialities'))->render();
                    $returnArray = array('tab' => 'specspeciality', 'html' => $returnHTML);
                    return \response()->json($returnArray);
                }
                Redis::hmset('user:'.Auth::user()->name.':where', $item->ParentTab, $id);
                $employees = (new Employee())->getFunction(Redis::get('dateFrom'), Redis::get('dateTo'), Redis::hget('user:'.Auth::user()->name.':where', 'ticketorganization'),$id);
                $dates = (new Date())->getFunction(Redis::get('dateFrom'), Redis::get('dateTo'), Redis::hget('user:'.Auth::user()->name.':where', 'ticketorganization'), $id);
                $returnHTML = view('ajax.employee')->with(compact('employees'))->render();
                $returnHTMLDates = view('ajax.date')->with(compact('dates'))->render();
                $returnArray = array('tab' => 'specfullname', 'html' => $returnHTML, 'datesHTML' => $returnHTMLDates);
            }
            // Обработчик третьей вкладки - Специалист
            elseif($item->ParentTab == 'specfullname') {
                if (isset($item->clear)) {
                    Redis::hdel('user:'.Auth::user()->name.':where', $item->tab);
                    $employees = (new Employee())->getFunction(Redis::get('dateFrom'), Redis::get('dateTo'), Redis::hget('user:'.Auth::user()->name.':where', 'ticketorganization'),  Redis::hget('user:'.Auth::user()->name.':where', 'specspeciality'));
                    $returnHTML = view('ajax.employee')->with(compact('employees'))->render();
                    $returnArray = array('tab' => 'specfullname', 'html' => $returnHTML);
                    return \response()->json($returnArray);
                }
                Redis::hmset('user:'.Auth::user()->name.':where', $item->ParentTab, $id);
                $dates = (new Date())->getFunction(Redis::get('dateFrom'), Redis::get('dateTo'), Redis::hget('user:'.Auth::user()->name.':where', 'ticketorganization'), Redis::hget('user:'.Auth::user()->name.':where', 'specspeciality'), $id);
                $returnHTML = view('ajax.date')->with(compact('dates'))->render();
                $returnArray = array('tab' => 'ticketdate', 'html' => $returnHTML);
            }
            // Обработчик четвертой вкладки - Дата
            elseif($item->ParentTab == 'ticketdate') {
                Redis::hmset('user:'.Auth::user()->name.':where', $item->ParentTab, $id);
                $times = (new EntireTable())->getFunctionTime(Redis::get('dateFrom'), Redis::get('dateTo'), Redis::hget('user:'.Auth::user()->name.':where', 'ticketorganization'), Redis::hget('user:'.Auth::user()->name.':where', 'specspeciality'), Redis::hget('user:'.Auth::user()->name.':where', 'specfullname'), $id);
                $returnHTML = \view('ajax.time')->with(compact('times'))->render();
                $returnArray = array('tab' => 'tickettime', 'html' => $returnHTML);
            }
            // Обработчик пятой вкладки - Время
            elseif($item->ParentTab == 'tickettime') {
                Redis::set('sessionStart', $this->timeNow);
                Redis::set('sessionFinishes', $this->timeNow->addMinutes($duration)->format('M d Y H:i:s'));
                Redis::hmset('user:'.Auth::user()->name.':where', $item->ParentTab, $id);
                $final = (new EntireTable())->getFunctionFinal(Redis::get('dateFrom'), Redis::get('dateTo'), Redis::hget('user:'.Auth::user()->name.':where', 'ticketorganization'), Redis::hget('user:'.Auth::user()->name.':where', 'specspeciality'), Redis::hget('user:'.Auth::user()->name.':where', 'specfullname'), Redis::hget('user:'.Auth::user()->name.':where', 'ticketdate'), $id);
                foreach ($final as $item) {
                    if($item->ticketstatus != 1) throw new \Exception('Статус номерка неактивен', 403);
                    Redis::set('appointmentticketid', $item->appointmentticketid);
                }
                // Блокируем строчку базы данных на время до $sessionFinishes
                DB::beginTransaction();
                try{
                    $newAppointment = EntireTable::where('appointmentticketid', Redis::get('appointmentticketid'))->first();
                    $newAppointment->ticketstatus = 3;
                    $newAppointment->blocktime = $this->timeNow->addMinutes($duration)->format('Y-m-d H:i:s');
                    $newAppointment->save();
                    DB::commit();
                }catch (\Exception $e) {
                    DB::rollback();
                    return $e;
                }catch (\Throwable $e) {
                    return $e;
                }
                $returnHTML = view('ajax.final')->with(compact('final'))->render();
                $returnArray = array('tab' => 'final', 'html' => $returnHTML, 'time' => Redis::get('sessionFinishes'));
            }
            // отправляем новые данные на фронт
            return \response()->json($returnArray);
        }
    }

    public function saveForm(Request $request){
        if($request->ajax()) {
            $ticketId = Redis::get('appointmentticketid');
            $newAppointment = (new EntireTable)->where('appointmentticketid', $ticketId)->first();
            $this->validate($request, $newAppointment->rulesSave);
            $data = $request->input();
            $newAppointment->patlastname = $surname = $data['surname'];
            $newAppointment->patfirstname = $name = $data['name'];
            $newAppointment->patpatronymicname = $thirdname = $data['thirdname'];
            $newAppointment->patfullname = $surname. ' ' .$name. ' ' .$thirdname;
            $newAppointment->patbirthdate = $dob = $data['dob'];
            $newAppointment->patpolicynum = $policenumber = $data['policenumber'];
            $newAppointment->patpolicydate = $policeexpires = $data['policeexpires'];
            $newAppointment->patphone = $tel = $data['tel'];
            $newAppointment->patemail = $email = $data['email'];
            $newAppointment->patvisitpurpose = $purpose = $data['purpose'];
            $newAppointment->patcomment = $extra = $data['extra'];
            $newAppointment->ticketstatus = 2;
            $newAppointment->blocktime = null;
            $newAppointment->ticketbookedby = Auth::user()->id;
            if($newAppointment->save()) {
                Redis::flushDB();
                $request->session()->flash('success', 'Номерок успешно сохранен');
                return \response()->json(array('success' => 'true', 'redirect' => '/find'));
            }else
                return \response()->json(array('success' => 'false'));
        }
    }

    public function saveDate(Request $request) {
        if($request->ajax() && $request->method('post')) {
            $this->validate($request, [
                'StartDate' => 'date',
                'EndDate' => 'date'
            ]);
            $startDate = $request->input('StartDate');
            $endDate = $request->input('EndDate');
            $dataTab = $request->input('dataTab');
            if($startDate <= $endDate) {
                Redis::set('dateFrom', $startDate);
                Redis::set('dateTo', $endDate);
                $appointments = new Appointment();
                $specialities = new Speciality();
                $employees = new Employee();
//                $dates = new Date();
                $appointments = $appointments->getFunction($startDate, $endDate);
                $specialities = $specialities->getFunction($startDate, $endDate, Redis::hget('user:'.Auth::user()->name.':where', 'ticketorganization'));
                $employees = $employees->getFunction($startDate, $endDate, Redis::hget('user:'.Auth::user()->name.':where', 'ticketorganization'), Redis::hget('user:'.Auth::user()->name.':where', 'specspeciality'))->sortBy('specfullname');
//                $dates = $dates->getFunction($startDate, $endDate, Redis::hget('user:'.Auth::user()->name.':where', 'ticketorganization'), Redis::hget('user:'.Auth::user()->name.':where', 'specspeciality'). Redis::hget('user:'.Auth::user()->name.':where', 'ticketdate'))->sortBy('ticketdate');
                $appointmentsHtml = view('ajax.org')->with(compact('appointments'))->render();
                $specialitiesHtml = view('ajax.speciality')->with(compact('specialities'))->render();
                $employeesHtml = view('ajax.employee')->with(compact('employees'))->render();
//                $datesHtml = view('ajax.date')->with(compact('dates'))->render();
                return response()->json(array(
                    'status' => 'success',
                    'appointments' => $appointmentsHtml,
                    'specialities' => $specialitiesHtml,
                    'employees' => $employeesHtml,
//                    'dates' => $datesHtml,
                    ));
            }else{
                return response()->json(array('status' => 'false', 'message' => 'Wrong dates arrgh'));
            }
        }
    }
}
