<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class D_speciality extends Model
{
    protected $table = 'd_speciality';
    protected $primaryKey = 'specialityid';

    public function entireTable() {
        return $this->hasOne('App\EntireTable', 'fk_spec', 'specialityid');
    }
}
