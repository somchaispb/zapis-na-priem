<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Speciality extends Model
{
    protected $table = 'v_spectab';

    /**
     * @param null $dateFrom
     * @param null $dateTo
     * @param null $conditions
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getFunction($dateFrom = null, $dateTo = null, $conditions = null) {
        $dateFrom = $dateFrom ? $dateFrom : Carbon::now()->format('Y-m-d');
        $dateTo = $dateTo ? $dateTo : Carbon::now()->addWeek(1)->format('Y-m-d');
        $str = is_array($conditions) ? implode(',', $conditions) : $conditions;
        return self::hydrate(DB::select(
            'select * from fn_spectab(\''.$dateFrom.'\'::date, \''.$dateTo.'\'::date, ARRAY['.$str.']::bigint[])'
        ));
    }

    /**
     * @param array $conditions
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function filter(array $conditions) {
        $str = implode(',', $conditions);
        return self::getFunction($str);
    }
}
