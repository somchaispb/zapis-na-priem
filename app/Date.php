<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
// TODO: convert php String Array to Varchar Postgres Array
class Date extends Model
{
    protected $table = 'fn_datetab';

    public function getFunction($dateFrom = null, $dateTo = null, $clinic = null, $spec = null, $empl = null)
    {
        $dateFrom = $dateFrom ? $dateFrom : Carbon::now()->format('Y-m-d');
        $dateTo = $dateTo ? $dateTo : Carbon::now()->addWeek(1)->format('Y-m-d');
        $clinic = is_array($clinic) ? implode(',', $clinic) : $clinic;
        $spec = is_array($spec) ? implode(',', $spec) : $spec;
        $empl = is_array($empl) ? implode(',', $empl) : $empl;
        if (!is_null($empl)) $empl =  '\'' . $empl .'\'';
        return self::hydrate(DB::select(
            'select * from fn_datetab(\''.$dateFrom.'\'::date, \''.$dateTo.'\'::date, ARRAY['.$clinic.']::bigint[], ARRAY['.$spec.']::bigint[], ARRAY['.$empl.']::varchar[])'
        ));
    }

}
