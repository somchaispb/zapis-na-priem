<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
// TODO: convert php String Array to Varchar Postgres Array
class EntireTable extends Model
{

    protected $table = 'appointmentticket';
    protected $primaryKey = 'appointmentticketid';
//    protected $dates =[
//        'ticketpublicationdate', 'ticketcreateddate'
//    ];
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [
        'patlastname',
        'patfirstname',
        'patpatronymicname',
        'patbirthdate',
        'patpolicynum',
        'patpolicydate',
        'patphone',
        'patemail',
        'patvisitpurpose',
        'patcomment'];
    public $rulesSave = [
        'surname' => 'required|alpha',
        'name' => 'required|alpha',
        'thirdname' => 'required|alpha',
        'dob' => 'required|date',
        'policenumber' => 'required|alpha_dash',
        'policeexpires' => 'required|date',
        'tel' => array('required', 'regex:/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/'),
        'email' => 'nullable|email',
        'purpose' => 'max:255',
        'extra' => 'max:255',
    ];
    public $rulesAdd = [
            'SpecLastName' => 'required|alpha',
            'SpecFirstName' => 'required|alpha',
            'SpecPatronymicName' => 'alpha',
            'SpecSpeciality' => 'numeric',
            'TicketService' => 'string|max:100',
            'TicketDate' => 'date',
            'TicketDate2' => 'date|after_or_equal:TicketDate',
            'TicketTimeFrom' => 'date_format:H:i',
            'TicketTimeTo' => 'date_format:H:i|after:TicketTimeFrom',
            'BreakFrom' => 'date_format:H:i',
            'BreakTo' => 'date_format:H:i|after:BreakFrom',
            'duration' => 'numeric',
            'finance' => 'string|max:100',
            'price' => 'string|max:100',
            'AddressNear' => 'string|max:100',
            'TimeForMoving' => 'string|max:50',
            'Recommendations' => 'string|max:255',
            'TicketOrganization' => 'numeric',
    ];
    public $rulesUpdate = [
        'SpecLastName' => 'alpha',
        'SpecFirstName' => 'alpha',
        'SpecPatronymicName' => 'alpha',
        'SpecSpeciality' => 'numeric',
        'patlastname' => 'required|alpha',
        'patfirstname' => 'required|alpha',
        'patpatronymicname' => 'required|alpha',
        'patbirthdate' => 'required|date',
        'patpolicynum' => 'required|alpha_dash',
        'patpolicydate' => 'required|date',
        'patphone' => array('required', 'regex:/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/'),
        'patemail' => 'nullable|email',
        'patvisitpurpose' => 'max:255',
        'patcomment' => 'max:255',
    ];


    public function d_speciality() {
        return $this->hasOne('App\D_speciality', 'specialityid', 'specspeciality');
    }

    public function user() {
        return $this->hasOne('App\User', 'id', 'ticketbookedby');
    }

    public function d_organization() {
        return $this->hasOne('App\D_organization', 'organizationid', 'ticketorganization');
    }

    public function getFunctionTime($dateFrom = null, $dateTo = null, $clinic = null, $spec = null, $empl = null, $date = null)
    {
        $dateFrom = $dateFrom ? $dateFrom : Carbon::now()->format('Y-m-d');
        $dateTo = $dateTo ? $dateTo : Carbon::now()->addWeek(1)->format('Y-m-d');
        $clinic = is_array($clinic) ? implode(',', $clinic) : $clinic;
        $spec = is_array($spec) ? implode(',', $spec) : $spec;
        $empl = is_array($empl) ? implode(',', $empl) : $empl;
        if (!is_null($empl)) $empl =  '\'' . $empl .'\'';
        return self::hydrate(DB::select(
            'select * from fn_timetab(\''.$dateFrom.'\'::date, \''.$dateTo.'\'::date, ARRAY['.$clinic.']::bigint[], ARRAY['.$spec.']::bigint[], ARRAY['.$empl.']::varchar[], \''.$date.'\'::date)'
        ));
    }

    public function getFunctionFinal($dateFrom = null, $dateTo = null, $clinic = null, $spec = null, $empl = null, $date = null, $time = null)
    {
        $dateFrom = $dateFrom ? $dateFrom : Carbon::now()->format('Y-m-d');
        $dateTo = $dateTo ? $dateTo : Carbon::now()->addWeek(1)->format('Y-m-d');
        $clinic = is_array($clinic) ? implode(',', $clinic) : $clinic;
        $spec = is_array($spec) ? implode(',', $spec) : $spec;
        $empl = is_array($empl) ? implode(',', $empl) : $empl;
        if (!is_null($empl)) $empl =  '\'' . $empl .'\'';
        return self::hydrate(DB::select(
            'select * from fn_finaltab(\''.$dateFrom.'\'::date, \''.$dateTo.'\'::date, ARRAY['.$clinic.']::bigint[], ARRAY['.$spec.']::bigint[], ARRAY['.$empl.']::varchar[],  \''.$date.'\'::date,  \''.$time.'\'::time)'
        ));
    }


}
