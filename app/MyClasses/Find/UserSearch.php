<?php

namespace App\MyClasses\Find;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class UserSearch
{
    public static function filter(Model $instance, array $array) {
        if(!is_array($array)) return false;
        $instance = (new $instance)->newQuery();
        foreach ($array as $key => $value) {
            if($value == null) continue;
            if(!empty($key)) $instance->where($key, $value);
        }
        return $instance;
    }

}
//
//(new Speciality)->hydrate(DB::select(
//    'select * from fn_spectab(cast('.Redis::get('t').' as date), cast('.Redis::get('t2').' as date))
//                    fn where fn.'.$item->ParentTab.' in ('.$id.')
//                    '
//));