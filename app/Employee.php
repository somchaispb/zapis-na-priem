<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    protected $table = 'fn_empltab';

    /**
     * @param null $dateFrom
     * @param null $dateTo
     * @param null $clinic
     * @param null $spec
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getFunction($dateFrom = null, $dateTo = null, $clinic = null, $spec = null) {
        $dateFrom = $dateFrom ? $dateFrom : Carbon::now()->format('Y-m-d');
        $dateTo = $dateTo ? $dateTo : Carbon::now()->addWeek(1)->format('Y-m-d');
        $clinic = is_array($clinic) ? implode(',', $clinic) : $clinic;
        $spec = is_array($spec) ? implode(',', $spec) : $spec;
        return self::hydrate(DB::select(
            'select * from fn_empltab(\''.$dateFrom.'\'::date, \''.$dateTo.'\'::date, ARRAY['.$clinic.']::bigint[], ARRAY['.$spec.']::bigint[])'
        ));
    }
}
